﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffects : MonoBehaviour {
    public float lifeTime = 10.0f;

    public Camera mainCam;

    public float camShakeStrength = 2;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);

      
    }

    private void Awake()
    {
        mainCam = Camera.main;

        camShakeStrength = camShakeStrength / Vector3.Distance(transform.position, mainCam.transform.position);

       // mainCam.GetComponent<CameraControl>().AddShake(camShakeStrength);
    }


}
