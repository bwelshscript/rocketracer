﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public float currentShakeAmount = 0;
    public float shakeReductionMult;
    public float maxShake;
    public Vector3 camTargetPos;
    public Vector3 camShakeOffset;

	// Use this for initialization
	void Start () {
        camTargetPos = transform.position;
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (currentShakeAmount >0)
        {
            currentShakeAmount -= Time.deltaTime * shakeReductionMult;

            camShakeOffset = new Vector3(Random.Range(-currentShakeAmount, currentShakeAmount), Random.Range(-currentShakeAmount, currentShakeAmount), Random.Range(-currentShakeAmount, currentShakeAmount));

            transform.position = camTargetPos + camShakeOffset;
        }

	}

    public void AddShake(float shakeAmount)
    {
        
         currentShakeAmount = Mathf.Clamp (currentShakeAmount+ shakeAmount, 0, maxShake) ; 

    }
}
