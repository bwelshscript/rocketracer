﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartFollow : MonoBehaviour {

    
    public Transform followTarget;
    public GameObject rocketModel;
    public Quaternion targetRotation;
    Quaternion startRot;
    public Vector3 followOffset;
    public float followStrength;
    public bool Leader = false;

    public float counterSwing = 0;
    public float counterSwingStrength;
    public float counterSwingRecoverStrength;
    public float maxCounterSwing = 2;
    private Vector3 newLerpHeightPos;
    private Vector3 newHeightPos;
    RaycastHit rayHit;

    // Use this for initialization
    void Start () {
		if (rocketModel != null) { startRot = rocketModel.transform.rotation; }
	}
	
	// Update is called once per frame
	void FixedUpdate () {

       

        counterSwing += Mathf.Lerp(0, -Input.GetAxis("Horizontal")*counterSwingStrength, Time.deltaTime );

       counterSwing= Mathf.Clamp(counterSwing, -maxCounterSwing, maxCounterSwing);
       counterSwing =  Mathf.MoveTowards(counterSwing, 0, counterSwingRecoverStrength);
        //  Debug.Log(counterSwing);

        //need to set z value to almost static, or clamped, x and y to variable
        
   

        if (Leader)
        {
            transform.rotation = followTarget.rotation;
            rocketModel.transform.rotation = Quaternion.Lerp(rocketModel.transform.rotation, targetRotation, Time.deltaTime * 3);

            newLerpHeightPos = Vector3.Lerp(transform.position, followTarget.position + followOffset, Time.deltaTime * followStrength);
            newHeightPos = followTarget.position + followOffset;
            transform.position = new Vector3(newHeightPos.x, newLerpHeightPos.y, newHeightPos.z);
        }
        else
        {
            transform.rotation = followTarget.rotation;
            transform.rotation = Quaternion.Lerp(transform.rotation, followTarget.transform.rotation, Time.deltaTime * followStrength);
            transform.position = followOffset + Vector3.Lerp(transform.position, followTarget.position + new Vector3(counterSwing, 0, 0), Time.deltaTime * followStrength);
        }

        if (Physics.Raycast(transform.position + (Vector3.up * 5), Vector3.down, out rayHit))
        {
            Debug.DrawRay(transform.position + (Vector3.up * 5), Vector3.down);
            if (rayHit.point.y > transform.position.y)
                transform.position = new Vector3(transform.position.x, rayHit.point.y, transform.position.z);
        }

    }
}
