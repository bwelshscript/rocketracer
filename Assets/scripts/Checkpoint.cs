﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

    RaceController rc;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
      //  Debug.Log(this.name + " was triggered by " + other.name);
        if (rc == null) { rc = FindObjectOfType<RaceController>(); }
       rc.CheckpointUpdate(other, GetComponent<BoxCollider>());
    }
}
