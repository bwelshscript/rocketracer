﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundThrustParticles : MonoBehaviour
{
    public RocketMovement rocketMovement;
    public GroundSplashBehaviour groundSplash;

    public GameObject rocketModel;
    bool updateSplashPos;
    public ParticleSystem gsps;
    public Color waterColour;
    public FindTerrainColour FTC;

    //Particle System Emission Variables. (ROT = Rate Over Time)
    private float ps_origMinROT = 500f;
    private float ps_origMaxROT = 800f;
    //private float origMaxParticles = 1000f;
    private float origSimSpeed = 1f;
    private float origRadius = 0.01f;

    private float ps_fastMaxROT = 3000f;

    private float fastSimSpeed = 2f;
    private float fastRadius = 50f;

    private float minspeed = 0f;
    private float maxSpeed = 800f;
    private float currentSpeed;

    public float speedNormalised;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateGroundSplashColour();
        currentSpeed = rocketMovement.currentRealSpeed;

        //Normalise Speed;
        speedNormalised = (currentSpeed - minspeed) / (maxSpeed - minspeed);

        #region Normalise Radius
        //var sh = gsps.shape;
        //sh.radius = (speedNormalised * (fastRadius - origRadius) + origRadius);
        #endregion

        #region Normalise Rate Over Time
        var m_emission = gsps.emission;
        m_emission.rateOverTime = (speedNormalised * (ps_fastMaxROT - ps_origMinROT) + ps_origMinROT);
        #endregion
    }

    void UpdateGroundSplashColour()
    {
        RaycastHit rayHit;
        Debug.DrawRay(rocketModel.transform.position, Vector3.down * 2, Color.red);
        if (Physics.Raycast(rocketModel.transform.position /*+ (transform.forward * groundSplashOffset) */, Vector3.down, out rayHit, 2))
        {
            //Attempts to make the rotation of the particle system the same as the terrain it's hitting.
            ////Do math to get the surface angle.
            //Vector3 incomingVec = rayHit.point - transform.position;
            //Vector3 reflectVec = Vector3.Reflect(incomingVec, rayHit.normal);
            //
            ////Change from Vector 3's to Quaternions.
            //Quaternion quat = Quaternion.identity;
            //quat.eulerAngles = new Vector3(reflectVec.x, reflectVec.y, reflectVec.z);
            //
            ////Once Surface is set up change from SandDust to tempParticleSystem.
            //groundSplash.transform.rotation = Quaternion.FromToRotation(transform.up, reflectVec) * transform.rotation; //Make the rotation of the particle system the surface slope.

            //gsps.Play(true);
            if (Vector3.Distance(rocketModel.transform.position, rayHit.point) < 2) //hoverHeightTarget +1)
            {

                groundSplash.transform.position = rayHit.point;


                if (!updateSplashPos) { gsps.Play(true); updateSplashPos = true; }


            }
            else if (updateSplashPos) { updateSplashPos = false; gsps.Play(false); return; }



            Renderer rend = rayHit.transform.GetComponent<Renderer>();
            MeshCollider meshCollider = rayHit.collider as MeshCollider;


            if (rend == null)
            {
                Terrain terr = rayHit.transform.GetComponent<Terrain>();
                // probs got a terrain instead
                if (terr != null)
                {


                    groundSplash.currentColour = FTC.getTerrainColour(rayHit.point, terr);


                    return;
                }

                return;
            }
            else if (rend.material == null)
            {
                return;
            }
            else if (rend.material.mainTexture == null)
            {
                // probs water. colour = blue / white
                groundSplash.currentColour = waterColour;

                return;
            }

            Texture2D tex = rend.material.mainTexture as Texture2D;
            Vector2 pixelUV = rayHit.textureCoord;
            pixelUV.x *= tex.width;
            pixelUV.y *= tex.height;
            groundSplash.currentColour = tex.GetPixel((int)pixelUV.x, (int)pixelUV.y);
        }
        else
        {
            gsps.Play(false);
        }


    }
}
