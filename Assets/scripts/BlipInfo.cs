﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class BlipInfo  {

  
    public Vector3 rotation;
    public Vector3 position;
    public float speed;
    public bool braking;
    public bool boosting;

}
