﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class RocketMovement : MonoBehaviour {

    [Header("basic rocket stats")]
    public float maxHealth;
    public float currentHealth;
    public float engineTemp;
    public float engineTempRiseRate;
    public float engineTempLowerRate;
    public float tempDmgThresh = 477;   //temps based on aluminum 7075, designed for aircraft apparently
    public float tempMaxThresh = 635;
    public float collisionDamageMultiplier = 0.2f; //this will be armour strength for collisions, can also be applied to incoming bullet/explosion damage
    public float engineDamageMultiplier = 0.03f; //this will be armour strength for collisions, can also be applied to incoming bullet/explosion damage
    public float windResistMult = 1;

    public float regenAmount;
    public int regenRate; //heal proc once every x frames
    [Range(0,100)]
    public float speedDropThreshholdBeforeColDamage = 150;

    [Header("input values")] //only visible for debugging purposes
    public float leftRight = 0; // -1 to 1
    public float thrust = 0;    // 0 to 1
    public bool boost = false;
    public bool brake = false;

    [Header("Rocket behaviour values")]
    public float speed = 10;
    private float startSpeed;
    public float currentRealSpeed;
    float turnAmount;
    public float maxSpeed;
    public float accelerationSpeed;
    public float rotateSpeed = 2;
    public float airBreakMult = 4;

    public float hoverHeightTarget;
    public float bouyancy;
    public float gravityStrength;
    private float currentGravStrength;
    private float turnDamp;
    public float[] raycastOffsets;
    Vector3 lastPosition;
    Rigidbody rb;
    public Camera mainCam;

    public float thrustLength;
    public Vector3 thrustOffset;
    public int thrustSmoothness = 10;

    [Header("exhaust trail systems and colours")]
    public TrailRenderer trail;
    public TrailRenderer boostTrail;
    public LineRenderer normalThrust;
    public LineRenderer boostThrust;
    public Color normalColour;
    public Color boostColour;
    public Color waterColour;
    float startWidth;

    public GameObject rocketModel;
   

    public float boostStrength { get; private set; }
    public PartFollow mainRocketModel;

    [Header("trailing cloud particle systems")]
    public GroundSplashBehaviour[] groundSplash;
    public ParticleSystem[] idleGsps;
    public ParticleSystem[] movingGsps;
    bool updateSplashPos;
    public float groundSplashOffset;
    public FindTerrainColour FTC; //find terrain colour object
    public GameObject softMaterialExplosionPrefab;

    [Header("engine fires & damage visibility systems")]
    public ParticleSystem engineFires_Moving;
    public ParticleSystem engineFires_Still;
    public ParticleSystem engineFiresWithTrail;

    float targetFireStrength = 0;
    float currentFireStrength = 0;
    Vector3 normalHit = Vector3.zero;

    [Header("Rocket death effects and changes")]
    public GameObject explosionPrefab;
    public GameObject[] toggleObjectsOnDeath;

    [HideInInspector]
    public bool alive = true;
    [HideInInspector]
    public bool canMove = false;


    float lastSpeed;

    //hide these
    [HideInInspector]
    public int healTimer = 60;
    [HideInInspector]
    public float averageSpeed, averageSpeed2, startMaxSpeed;
    [HideInInspector]
    public bool canBoost,breaking;
    float[] speedAverages;
    bool takenColDam = false;
    int colDamTimer = 60;
    bool cameToStop = false;


    //boyancy values;
    float speedMod = 0;
    bool rising = false;
    bool falling = false;
    float fallStrength = 0;
    public LayerMask boyancyCollisionLayers;

   

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(rocketModel.transform.position + thrustOffset, 0.05f);
    }

    // Use this for initialization
    void Start () {

       
        startMaxSpeed = maxSpeed;
        rb = GetComponent<Rigidbody>();
        startSpeed = speed;
        mainCam = Camera.main;
        startWidth = trail.startWidth;
       // idleGsps = groundSplash.GetComponent<ParticleSystem>();
        currentHealth = maxHealth;
        engineTemp = 100;
        RunFires();
        currentRealSpeed = 0;
        lastSpeed = 0;

        normalThrust.positionCount = thrustSmoothness;
        boostThrust.positionCount = thrustSmoothness;


        speedAverages = new float[60];

        for (int i = 0; i < speedAverages.Length; i++)
        {
            speedAverages[i] = 0;
        }
    }

    private void Update()
    {        
        UpdateGroundSplashColour();
    }

    // Update is called once per frame
    void FixedUpdate () {

        averageSpeed = CalculateAverageSpeed();

        if (averageSpeed <= 1 && !cameToStop) { speed = 0; cameToStop = true; }
        if (cameToStop && averageSpeed > 10 ) { cameToStop = false; }



        if (!takenColDam && canMove && alive && canMove && averageSpeed2 < averageSpeed )
        {

            float speedDifPercent = (( averageSpeed - averageSpeed2 ) / averageSpeed)*100;

            //we just came to a very sudden stop, we should be dead at this point.
            if (speedDifPercent > speedDropThreshholdBeforeColDamage )
            {
                float speedDiff = Mathf.Abs(averageSpeed - averageSpeed2);
                currentHealth -= speedDiff * collisionDamageMultiplier;
                takenColDam = true;
                colDamTimer = 30;
              //  Debug.Log("speed diff was " + speedDifPercent + "\n damage received " + speedDiff * collisionDamageMultiplier + "\n average speed " + averageSpeed + "\n averageSpeed2 " + averageSpeed2 + "\n actual speed " + currentRealSpeed);
            }

        }

        if (takenColDam && colDamTimer > 0)
        {
            colDamTimer--;
        }
        else { takenColDam = false; }

        //calculate real speeds
        Vector3 lastPos2  = new Vector3(lastPosition.x, lastPosition.y * 0.5f, lastPosition.z)  ;
        Vector3 curPos2 = new Vector3(transform.position.x, transform.position.y * 0.5f, transform.position.z);
        currentRealSpeed = ((Vector3.Distance(lastPos2, curPos2) / Time.fixedDeltaTime) * 0.5f) * 6 ;
     
        //health regen
        if ( currentHealth < maxHealth && healTimer <= 0)
        {
            currentHealth += regenAmount; healTimer = 60;
            regenAmount = Mathf.Clamp(regenAmount, 0, maxHealth);
        }
        else { healTimer--; }

        CheckFutureCollision();

        //are we dead?
        if (currentHealth <= 0 && alive)
        {
            Explosion();           
        }
        else if (alive)
        {
            Height();
            if (canMove)
            {
                Rotation();
                Movement();
            }
    
            UpdateTrails();
            CalculateEngineHeat();

            if (!brake) { breaking = false; }  
            lastPosition = transform.position;
            lastSpeed = currentRealSpeed;

            if (canBoost && engineTemp > tempDmgThresh && !boost)
            {
                canBoost = false;
            }
            else if (!canBoost && engineTemp < tempDmgThresh)
            {
                canBoost = true;
            }
        }
    }

    float CalculateAverageSpeed()
    {
        float averageSpeed = 0;

        for (int i = 0; i < speedAverages.Length -1; i++)
        {
            averageSpeed += speedAverages[i];

            if (i > 54)
            { averageSpeed2 += speedAverages[i]; }

            speedAverages[i] = speedAverages[i+1];
       

        }

        speedAverages[speedAverages.Length-1] = currentRealSpeed;
        
        averageSpeed =Mathf.Abs( averageSpeed / speedAverages.Length);
        averageSpeed2 = Mathf.Abs(averageSpeed2 / 5);
       //  Debug.Log(averageSpeed);
        return averageSpeed;
    }

    void CalculateEngineHeat()
    {
        if ((thrust > 0.2f)) // we are accelerating, engine temp rises
        {
            if (boost && canMove && canBoost) // we are also boosting, engine temp quickly rises, also above safe operating temperatures
            {
                if (engineTemp < tempDmgThresh) // heat increases quickly while in safe temp, slowly otherwise
                {
                    engineTemp += engineTempRiseRate * 4;
                }
                else
                {
                    engineTemp += engineTempRiseRate * 2;
                }
            }
            else // normal temp rise, stays within safe temp range
            {             
                if (engineTemp < tempDmgThresh)
                {
                    engineTemp += engineTempRiseRate;
                }

                if (engineTemp > 160) //if we are above a lower temp threshhold, reduce temp. this is for cooling system upgrades later on
                {                                //as the boosting function is a strong part of the gameplay experience;

                    if (engineTemp > tempDmgThresh)
                    { engineTemp -= engineTempLowerRate * 4; }
                    else { engineTemp -= engineTempLowerRate; }
                }
            }       
        }
        else if (engineTemp > 160) 
        {        
            

            engineTemp -= engineTempLowerRate * 5;
        }

        //sitting still and firing the laser should still kill a player, so this.
        if (thrust > 0.2f || engineTemp > 1100)
        {
            // only apply damage if we are boosting or temp is too high, assume the engine prioritizes cooling when not accelerating (speed reduces heavily anyway)
            if (engineTemp >= tempDmgThresh) { currentHealth -= (engineTemp - tempDmgThresh * engineDamageMultiplier) * 0.00006f; }

            if (engineTemp >= tempMaxThresh) { currentHealth -= (engineTemp - tempDmgThresh * engineDamageMultiplier) * 0.00002f; }
        }
      

        if (engineTemp < tempMaxThresh) {}
        // do fire visibility
        RunFires();
    }

    void RunFires()
    {
    
        if (engineFires_Moving != null)
        {
     
            ParticleSystem.EmissionModule efmM = engineFires_Moving.emission;
            ParticleSystem.EmissionModule efmS = engineFires_Still.emission;
            ParticleSystem.EmissionModule efmT = engineFiresWithTrail.emission;

            // get health percentage first, fires should only begin showing at about 50%

            float normalizedAMT = (currentHealth  / maxHealth ) ;


           // Debug.Log(normalizedAMT);
            if (normalizedAMT < 0.3f)
            {
               // normalizedAMT = ((normalizedAMT*100) / (0.333f));
                normalizedAMT = 1 - normalizedAMT;
                targetFireStrength = 1;
            }
            else
            {
                normalizedAMT = 0;
                targetFireStrength = 0;
            }

           

           if (currentRealSpeed > 50 )
            {
                efmM.rateOverDistance = Mathf.Lerp(efmM.rateOverDistance.constant, normalizedAMT * 100, Time.deltaTime * 10 );
                efmS.rateOverTime = 0;
             
            }
            else
            {
                efmS.rateOverTime = normalizedAMT * 100;
                efmM.rateOverDistance = normalizedAMT * 10;
      
            }

            efmT.rateOverTime = normalizedAMT * 10;
        }

        if (currentFireStrength != targetFireStrength)
        {
            Color oldColour;
            currentFireStrength = Mathf.Lerp(currentFireStrength, targetFireStrength, Time.deltaTime * 3);

          //  foreach (MeshRenderer item in engineFirePLanes.GetComponentsInChildren<MeshRenderer>())
          //  {
               // oldColour = item.material.GetColor("_Color");
               // item.material.SetColor("_Color", new Color(oldColour.r, oldColour.g, oldColour.b, currentFireStrength));
          //  }
        }


    }

    void Explosion()
    {
      //  Debug.Log(currentHealth);
        Instantiate(explosionPrefab, transform.position, transform.rotation, null);
        foreach (GameObject item in toggleObjectsOnDeath)
        {
            item.SetActive(false);
        }

        alive = false;
       
    }

    public static Vector3[] MakeSmoothCurve(Vector3[] arrayToCurve, int smoothness)
    {
        List<Vector3> points;
        List<Vector3> curvedPoints;
        int pointsLength = 0;
        int curvedLength = 0;

        if (smoothness < 1.0f) smoothness = 1;

        pointsLength = arrayToCurve.Length;

        curvedLength = smoothness;
        curvedPoints = new List<Vector3>(curvedLength);

        float t = 0.0f;
        for (int pointInTimeOnCurve = 0; pointInTimeOnCurve < curvedLength + 1; pointInTimeOnCurve++)
        {
            t = Mathf.InverseLerp(0, curvedLength, pointInTimeOnCurve);

            points = new List<Vector3>(arrayToCurve);

            for (int j = pointsLength - 1; j > 0; j--)
            {
                for (int i = 0; i < j; i++)
                {
                    points[i] = (1 - t) * points[i] + t * points[i + 1];
                }
            }

            curvedPoints.Add(points[0]);
        }

        return (curvedPoints.ToArray());
    }

    Vector3[] CalcLrpPos()
    {
        Vector3[] ThrustPositions = new Vector3[3];
        ThrustPositions[0] = Vector3.zero; // start pos
        ThrustPositions[1] = ThrustPositions[0] - (transform.forward * 0.1f);
        ThrustPositions[2] = ThrustPositions[1] - rb.velocity;

        return MakeSmoothCurve(ThrustPositions, thrustSmoothness -1);
        
    }

    void UpdateTrails()
    {

        if (!breaking && boost && canBoost)
        {
            trail.startWidth = Mathf.Lerp(trail.startWidth, 0, Time.deltaTime * 3);
            normalThrust.startWidth = trail.startWidth;
            boostTrail.startWidth = Mathf.Lerp(boostTrail.startWidth, startWidth * 2, Time.deltaTime * 3);
            boostThrust.startWidth = boostTrail.startWidth;
            trail.material.color = boostColour;
        }
        else
        {
            trail.startWidth = Mathf.Lerp(trail.startWidth, startWidth, Time.deltaTime);
            normalThrust.startWidth = trail.startWidth;
            boostTrail.startWidth = Mathf.Lerp(boostTrail.startWidth, 0, Time.deltaTime);
            boostThrust.startWidth = boostTrail.startWidth;
            trail.material.color = normalColour;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // TODO check if we are colliding with water or terrain (sand)


        if (speed > 1000)
        {
            GameObject prefab = Instantiate(softMaterialExplosionPrefab) as GameObject;

            prefab.transform.position = collision.contacts[0].point + (transform.forward * 2);
    
            prefab.transform.rotation = transform.rotation;

            prefab.GetComponentInChildren<Rigidbody>().velocity = rb.velocity;
            ParticleSystem.MainModule ps = prefab.GetComponentInChildren<ParticleSystem>().main;
            ParticleSystemRenderer psr = prefab.GetComponentInChildren<ParticleSystemRenderer>();

            ps.startColor = groundSplash[0].currentColour;
            foreach (Material mat in psr.materials)
            {
                mat.color = groundSplash[0].currentColour;
            }


        }

    }

    void UpdateGroundSplashColour() //and position, also its the trailing cloud, not splash. splash grabs its colour from this though.
    {
        RaycastHit rayHit;
        Debug.DrawRay(rocketModel.transform.position, Vector3.down * 2, Color.red);
        if (Physics.Raycast(rocketModel.transform.position + (transform.forward * groundSplashOffset), Vector3.down, out rayHit, 2))
        {
            if (Vector3.Distance(rocketModel.transform.position, rayHit.point) < hoverHeightTarget +1)
            {
                //update positions
                foreach (GroundSplashBehaviour item in groundSplash)
                {
                    item.transform.position = rayHit.point;
                }
              


                
                    if (averageSpeed > 10)
                    {
                        foreach (ParticleSystem item in idleGsps)
                        {
                            item.Play(true); 
                        }
                        foreach (ParticleSystem item in movingGsps)
                        {
                            item.Play(false);
                        }

                      
                    }
                    else
                    {
                        foreach (ParticleSystem item in idleGsps)
                        {
                            item.Play(false);
                        }
                        foreach (ParticleSystem item in movingGsps)
                        {
                            item.Play(true);
                        }
                    }

                    updateSplashPos = true;
                


            }
            else if (updateSplashPos)
            {

                foreach (ParticleSystem item in idleGsps)
                {
                    item.Play(false);
                }
                foreach (ParticleSystem item in movingGsps)
                {
                    item.Play(false);
                }

                updateSplashPos = false;
                return;
            }

            Renderer rend = rayHit.transform.GetComponent<Renderer>();
            MeshCollider meshCollider = rayHit.collider as MeshCollider;

            if (rend == null)
            {
                Terrain terr = rayHit.transform.GetComponent<Terrain>();
                // probs got a terrain instead
                if (terr != null)
                {

                    foreach (GroundSplashBehaviour item in groundSplash)
                    {
                        item.currentColour = FTC.getTerrainColour(rayHit.point, terr);
                    }

                    return;
                }

                return;
            }

            else if (rend.material == null)
            {
                return;
            }

            else if (rend.material.mainTexture == null)
            {
                // probs water. colour = blue / white
                foreach (GroundSplashBehaviour item in groundSplash)
                {
                    item.currentColour = waterColour;
                }
                return;
            }

            Texture2D tex = rend.material.mainTexture as Texture2D;
            Vector2 pixelUV = rayHit.textureCoord;
            pixelUV.x *= tex.width;
            pixelUV.y *= tex.height;
            foreach (GroundSplashBehaviour item in groundSplash)
            {
                item.currentColour = tex.GetPixel((int)pixelUV.x, (int)pixelUV.y);
            }
        }


    }

    void Rotation()
    {
        // turning rotation
        Quaternion tempNorm;
        // need to add in a ramp up on turning, to make digital input feel as good as analogue input
        RaycastHit rayHit;
        
        if (Physics.Raycast(transform.position + (transform.forward * raycastOffsets[0]), Vector3.down, out rayHit, hoverHeightTarget +3))
        {
            tempNorm = Quaternion.Lerp(mainRocketModel.targetRotation, Quaternion.FromToRotation(transform.up, rayHit.normal) * transform.rotation, Time.deltaTime * 20);
            normalHit =  tempNorm.eulerAngles  ;
        }
        else
        {
            tempNorm = Quaternion.Lerp(mainRocketModel.targetRotation, Quaternion.identity, Time.deltaTime * 4);
            normalHit = tempNorm.eulerAngles;
        }

        if (!breaking && ( boost)) { turnDamp = 0.1f; }
        else if (breaking && !boost && Mathf.Abs( leftRight ) > 0.2f)
        {
            //ADD TURNING FORCE
            rb.AddForce(transform.right*( leftRight * (speed * 0.001f )), ForceMode.Force);
        }
        else { turnDamp = 1; }


        turnAmount = Mathf.Lerp(0, leftRight * (rotateSpeed * turnDamp), Time.deltaTime * 3);
        float overturnAmount = Mathf.Lerp(0, turnAmount , Time.deltaTime * 3);

       

        rb.MoveRotation(Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y + (turnAmount), 0)));
        mainRocketModel.targetRotation = Quaternion.Euler(new Vector3(normalHit.x , transform.rotation.eulerAngles.y + overturnAmount, normalHit.z ));
                                                        

    }

    void Height()
    {
        float newHeight = 0;
        RaycastHit rayHit;
       

        foreach (float item in raycastOffsets)
        {
            if (Physics.Raycast(transform.position + (transform.forward * item) + transform.up, Vector3.down, out rayHit, Mathf.Infinity, boyancyCollisionLayers))
            {
                Debug.DrawRay(transform.position + (transform.forward * item), Vector3.down);
                newHeight += Vector3.Distance(transform.position + (transform.forward * item), rayHit.point);
            }           
        }
        newHeight /= raycastOffsets.Length;


        if (currentRealSpeed > 10) { speedMod = 1; }

        if (newHeight < hoverHeightTarget)
        {
            rb.useGravity = false;
            rb.AddForce(transform.up * bouyancy * speedMod, ForceMode.Force);
            currentGravStrength = 0;
        }
        else
        {
            //  rb.useGravity = true;
            currentGravStrength += (gravityStrength + currentGravStrength) * Time.deltaTime;
            rb.AddForce(-transform.up * currentGravStrength * speedMod, ForceMode.Force);
        }



    }

    void Movement()
    {
        //airbrake reduction
        if (speed > 0 && brake) { speed -= accelerationSpeed * airBreakMult; breaking = true; }

        //if we are under max speed in normal accel, increase accel
        else if (speed < maxSpeed &&( thrust> 0))
        {
            if (boost && canBoost)
            {
                maxSpeed = startMaxSpeed * 2;
                speed += (accelerationSpeed * thrust);
            }
            else
            {
                maxSpeed = startMaxSpeed;
                speed += (accelerationSpeed * thrust);
            }
        }

       
       //if no input, reduce speed
        else if (speed > 0)
        {
            if (speed > maxSpeed) { speed -= (accelerationSpeed * 6) * windResistMult; }
            else { speed -= accelerationSpeed * windResistMult; }
        }


        // cuts off overall acceleration on hard turns, calms it down a bit on soft turns
        float accelerationClamp = Mathf.Clamp(15 - currentRealSpeed, 5,15);

        if (!boost)
        {

            if (Mathf.Abs(leftRight) >= 0.25f && Mathf.Abs(leftRight) <= 0.4f)
            { speed = Mathf.MoveTowards(speed, startSpeed, (accelerationClamp * 0.25f)); }  //light turning only reduces speed by a quater measure
            else if (Mathf.Abs(leftRight) >= 0.41f)
            { speed = Mathf.MoveTowards(speed, startSpeed, (accelerationClamp)); }

            //acceleration should be cut off if we turn so hard that we stop moving/ we just stop moving from like hitting something

            //turning strength should be reduced as we go faster

            // booster
        } else if (!breaking  )
        {
            speed += accelerationSpeed * boostStrength;

        }
        
        // add movement to rocket
        Vector3 newMoveVec;
        if (Mathf.Abs(leftRight) >= 0.25f && speed < 200) { speed = speed + 200; }
        newMoveVec = transform.forward * (speed * Time.deltaTime);
        rb.AddForce (newMoveVec , ForceMode.Force);

    }

    void CheckFutureCollision()
    {
        // check to see if we will tunnel through an object next frame
        // by doing a spherecast along our trajectory * by our current speed in unity units.
        // if we find a collision, check to see what it is. if its something like the terrain then we apply damage and probably die
        // if its a destructable object (like trees or flags or maybe players) then we apply damage to both parties
        // westop the player from moving into the object if needed

        Vector3 direction =   transform.position - lastPosition;
        float distance = Vector3.Distance(lastPosition, transform.position) * 1.5f;
        RaycastHit hitInfo;
        
        if (Physics.SphereCast(transform.position, 0.2f, -direction, out hitInfo, distance))
        {
            Debug.DrawRay(transform.position, direction, Color.red);

            if (currentRealSpeed > 60 && hitInfo.collider.tag == "Terrain")
            {



                float angle = Vector3.Angle(Vector3.up, hitInfo.normal) * Mathf.Deg2Rad;

                if (!takenColDam)
                {
                    if (angle > 0.95f)
                    {
                        // Debug.DrawLine(transform.position, hitInfo.point, Color.red,999);
                        currentHealth -= (currentRealSpeed * collisionDamageMultiplier * 0.15f);
                        speed *= 0.85f;
                        // Debug.Log(angle.ToString("0.00"));
                        takenColDam = true;
                        colDamTimer = 30;
                    }
                    else if (angle > 0.75f)
                    {
                        currentHealth -= (currentRealSpeed * (collisionDamageMultiplier * 0.05f));
                        // Debug.DrawLine(transform.position, hitInfo.point, Color.white,999);
                        // Debug.Log(angle.ToString("0.0"));
                    }
                    takenColDam = true;
                    colDamTimer = 30;
                }
            }

        }

    }

    public void ReceiveDamage(int amount)
    {
        currentHealth -= amount;
    }

    public void ReceiveHeatDamage(int amount)
    {
        engineTemp += amount;
    }
}
