﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

    public GameObject target;
    public GameObject closeTarget;
    public GameObject farTarget;

    public GameObject lookAtTarget;
    public Vector3 lookPositionOffset;
    public Vector3 followPositionOffset;
    Vector3 startFollowPosOffset;
    Vector3 newFollowPOsOffset;
    public float lerpSpeedMult = 2;
    private Quaternion oldPosition;
    private Quaternion newPosition;
    Vector3 newLerpHeightPos;
    Vector3 newHeightPos;
    public float minCamHeight;

    // lerpToNewTarget 
    float racelerpFloat = 0;
    float lerpToTargetMult = 0.75f;
    float lerpToTargetTime = 0;
    bool doingTransition = false;
    bool completedTransition;
    bool overrideLook = false;
    Vector3 oldTrackingPos;
    Vector3 lerpedTrackingPos;

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(target.transform.position + followPositionOffset, 0.25f);
    }

    // Use this for initialization
    void Start () {
        startFollowPosOffset = followPositionOffset;
	}

    public void ToggleCloseTarget(float mult)
    {
        newFollowPOsOffset = startFollowPosOffset +( target.transform.forward * mult);
    }
	
    public void LerpToNewTarget(GameObject newTarget)
    {
        racelerpFloat = 0;
        oldTrackingPos = target.transform.position;
        target = newTarget;
        doingTransition = true;
        lerpToTargetTime = 0;
    }

    public void UpdateLerp()
    {
        lerpToTargetTime += Time.deltaTime * lerpToTargetMult;
        newHeightPos = Vector3.Lerp(oldTrackingPos, target.transform.position, lerpToTargetTime) + followPositionOffset;

        if (lerpToTargetTime >= 1) { doingTransition = false; }
    }

	// Update is called once per frame
	void FixedUpdate () {

        if (doingTransition) { UpdateLerp(); }

        newLerpHeightPos = Vector3.Lerp(transform.position, target.transform.position + followPositionOffset, Time.deltaTime * lerpSpeedMult);
        if (!doingTransition) { newHeightPos = target.transform.position + followPositionOffset; }    
        transform.position = new Vector3(newHeightPos.x, newLerpHeightPos.y, newHeightPos.z);

        // override min cam height from ground here
        RaycastHit hit;
        Physics.Raycast(transform.position, Vector3.down, out hit);
        if (Vector3.Distance(transform.position, hit.point) < minCamHeight)
        {
            transform.position = new Vector3(transform.position.x, hit.point.y + minCamHeight, transform.position.z);
        }

        if (!overrideLook)
        { UpdateCameraLook(); }

        if (followPositionOffset != newFollowPOsOffset)
        {
            followPositionOffset = Vector3.Lerp(followPositionOffset, newFollowPOsOffset, Time.deltaTime);
        }
	}

    void UpdateCameraLook()
    {
        oldPosition = transform.rotation;
        transform.LookAt(lookAtTarget.transform.position + lookPositionOffset);
        newPosition = transform.rotation;
        transform.rotation = Quaternion.Lerp(oldPosition, newPosition, Time.deltaTime * lerpSpeedMult );

    }

    public void RaceLook()
    {
        racelerpFloat += Time.deltaTime * 3 ;
        transform.rotation = Quaternion.Lerp(transform.rotation,
           Quaternion.Euler(  new Vector3(target.transform.rotation.eulerAngles.x, target.transform.rotation.eulerAngles.y +180, target.transform.rotation.eulerAngles.z) ) , 
            racelerpFloat);
       
    }
}
