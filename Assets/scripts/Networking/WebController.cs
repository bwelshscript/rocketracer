﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class WebController : MonoBehaviour
{
    [SerializeField]
    bool currentlyUploading = false;

    public string name = "default";
    public int time = 1500;
    public int difficulty = 1;
    public int placement = 1;
    public int vehicleID = 0;
    public int mapID = 0;

	// Update is called once per frame
	void Update () {
		if (!currentlyUploading && Input.GetKeyDown(KeyCode.Backslash))
        {
            SubmitScore(name, time, difficulty, placement, vehicleID, mapID);
        }
	}

    void SubmitScore (string _name, int _time, int _difficulty, int _placement, int _vehicleID, int _mapID)
    {
        StartCoroutine(UploadScore(name, time, difficulty, placement, vehicleID, mapID));
    }

    IEnumerator UploadScore(string _name, int _time, int _difficulty, int _placement, int _vehicleID, int _mapID)
    {
        currentlyUploading = true;

        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();

        formData.Add(new MultipartFormDataSection("name", _name));
        formData.Add(new MultipartFormDataSection("time", _time.ToString()));
        formData.Add(new MultipartFormDataSection("difficulty", _difficulty.ToString()));
        formData.Add(new MultipartFormDataSection("placement", _placement.ToString()));
        formData.Add(new MultipartFormDataSection("vehicleid", _vehicleID.ToString()));
        formData.Add(new MultipartFormDataSection("mapid", _mapID.ToString()));

        UnityWebRequest www = UnityWebRequest.Post("https://rocketracerdb.000webhostapp.com/submit-score.php", formData);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            Debug.Log("Success! Form upload complete");
            currentlyUploading = false;
            Debug.Log(www.downloadHandler.text);
        }
    }
}
