﻿using System.Collections;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;


public class populateTerrain : EditorWindow {

    GameObject baseTerrain;
    GameObject tempGameObject;
    Terrain tempTerrain;
    // string myString = "Hello World";
    // bool groupEnabled;
    //  bool myBool = true;
    // float myFloat = 1.23f;
    string notification = "Base Terrain has no Terrain Component, failed to run";
   
    Vector2 TerrainsWxH;

    [MenuItem("Tools/Instantiate Terrains")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(populateTerrain));
    }


    void OnGUI() {

        GUILayout.Label("Populate Terrains", EditorStyles.largeLabel);
        GUILayout.Label("Base Terrain", EditorStyles.label);
        baseTerrain = EditorGUILayout.ObjectField(baseTerrain,typeof(Object),true) as GameObject;

        TerrainsWxH = EditorGUILayout.Vector2Field("width and height", TerrainsWxH);

       if(  GUILayout.Button("Place Terrains"))
        {
            Terrain test = baseTerrain.GetComponent<Terrain>();


            if (test == null)
            {
                Debug.LogWarning(notification); return;
            }
            else
            {
                GenerateTerrain();
            }
        }
    }

    public static void CopyFrom<T1, T2>(T1 obj, T2 otherObject)
       where T1 : class
       where T2 : class
    {
        PropertyInfo[] srcFields = otherObject.GetType().GetProperties(
            BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);

        PropertyInfo[] destFields = obj.GetType().GetProperties(
            BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);

        foreach (var property in srcFields)
        {
            var dest = destFields.FirstOrDefault(x => x.Name == property.Name);
            if (dest != null && dest.CanWrite)
                dest.SetValue(obj, property.GetValue(otherObject, null), null);
        }
    }

    void GenerateTerrain()
    {
        TerrainData terData = new TerrainData();

        for (int i = 0; i < TerrainsWxH.x; i++)
        {
            for (int j = 0; j < TerrainsWxH.y; j++)
            {

                tempGameObject = Instantiate(baseTerrain,new Vector3(500 * i, 0, 500 *j), Quaternion.identity , null) as GameObject;
                tempTerrain = tempGameObject.GetComponent<Terrain>();

                //apply naming
                tempTerrain.name = SceneManager.GetActiveScene().name + "_Terrain_Tile_" + i + "_" + j;
                
                //set up net terrain data
                Terrain terrain = baseTerrain.GetComponent<Terrain>();
                terData = new TerrainData();
                CopyFrom(terData, terrain.terrainData);  // copy the basic getable/setable properties

                string filePath = "Assets/GeneratedTerrainData/" + SceneManager.GetActiveScene().name;

               

                if (!Directory.Exists(filePath))
                {
                    Debug.Log(filePath);
                    AssetDatabase.CreateFolder("Assets/GeneratedTerrainData", SceneManager.GetActiveScene().name);
                }

                //apply name and save new terrain data to its own folder
                AssetDatabase.CreateAsset(terData, filePath+ "/" + tempTerrain.name + "_terrainData.asset");

                //apply terrain data to terrain and collider
                tempTerrain.terrainData = terData;
                tempTerrain.terrainData.name = tempTerrain.name + "_terrainData";
                TerrainCollider tc = tempGameObject.GetComponent<TerrainCollider>();
                tc.terrainData = tempTerrain.terrainData;

            }
        }

     
    }
    
}
