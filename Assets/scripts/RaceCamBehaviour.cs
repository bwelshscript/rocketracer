﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceCamBehaviour : MonoBehaviour {

    public RaceController RC;
    public CamFollow camFol;
    public Camera raceCam;
    public GameObject[] toggleItems;
    public Behaviour[] toggleComponents;
    public bool toggleItemsOn = true;
    int currentTrackingIndex = 0;
    AudioListener AL;



    // Use this for initialization
    void Start () {
        AL = GetComponent<AudioListener>();
        raceCam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {

        SetActives();
        if (!toggleItemsOn)
        { UpdateTrackingTarget(); }

	}

    void UpdateTrackingTarget()
    {
        if (RC.playerInFirst != currentTrackingIndex)
        {
          
            currentTrackingIndex = RC.playerInFirst;
            GameObject tempRacer = RC.racers[currentTrackingIndex];
            camFol.LerpToNewTarget(Helper.FindComponentInChildWithTag<Transform>(tempRacer.transform.parent.gameObject, "CameraTarget").gameObject);
           // camFol.target = 
            camFol.lookAtTarget = tempRacer;
            Debug.Log(currentTrackingIndex);
        }
        camFol.RaceLook();
    }



    void SetActives()
    {
        if (toggleItemsOn && !toggleItems[0].activeSelf)
        {
            raceCam.enabled = false;
            foreach (GameObject item in toggleItems)
            {
                item.SetActive(true);
                AL.enabled = false;
            }
            foreach (Behaviour item in toggleComponents)
            {
                item.enabled = false;             
            }
        }
        else if (!toggleItemsOn && toggleItems[0].activeSelf)
        {
            raceCam.enabled = true;
            foreach (Behaviour item in toggleComponents)
            {
                item.enabled = true;
            }
            foreach (GameObject item in toggleItems)
            {
                item.SetActive(false);
                AL.enabled = true;
            }

        }
    }
}
