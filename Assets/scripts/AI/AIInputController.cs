﻿using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using UnityEngine;

public class AIInputController : MonoBehaviour {

     List< BlipInfo> blipArray = new List<BlipInfo>();
    public RocketMovement RM;
    public GunControll GC;
    public int currentBlip = 0;
    public float minBlipDist = 0.2f;
    public float currentBlipDist = 0;
    float lastBlipDist = 0;
    public float rotAngle = 0;

    public float maxAcceptableEngineTemp = 900;
    public float lowestAcceptableHealth = 40;

 

    // Use this for initialization
    void Start () {
       

    }
	
	// Update is called once per frame
	void Update () {

        if ( blipArray.Count > 0)
        { DoInput(); }
        
        ChooseBlip();

        if (GC != null) { GC.fireWeapons = Input.GetButton("Fire1"); }
    }

    public void ReceiveBlips(BlipInfo[] receivedBlips)
    {


        //  blipArray = receivedBlips.ToList();
        foreach (BlipInfo item in receivedBlips)
        {
            blipArray.Add(item);
        }

      //  Debug.Log(blipArray.Count);
       
    }
    
    void ChooseBlip()
    {
        //if we are close to a blip, choose the next blip
        currentBlipDist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(blipArray[currentBlip].position.x,
            blipArray[currentBlip].position.z));

        if (Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(blipArray[currentBlip].position.x,
            blipArray[currentBlip].position.z)) < minBlipDist)
        {
            if (currentBlip == blipArray.Count) { currentBlip = 0; }
            else {
                currentBlip++;
                currentBlipDist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(blipArray[currentBlip].position.x,
                blipArray[currentBlip].position.z));
                lastBlipDist = currentBlipDist;
                return;
            }
        }

        //but! if we are moving away from the blip, then instead choose the blip we are closest to
        if (lastBlipDist < currentBlipDist)
        {
            float shortestDist = 100;
            int bestIndex = currentBlip;
            // find nearest blip;
            for (int i = 0; i < blipArray.Count; i++)
            {
                if (Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(blipArray[i].position.x,
                     blipArray[i].position.z)) < shortestDist)
                {
                    shortestDist = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(blipArray[i].position.x,
                     blipArray[i].position.z));
                    bestIndex = i;
                }
            }

            if (bestIndex > currentBlip)
            { currentBlip = bestIndex; }
            else
            {
               // currentBlip++;
            }
        }

        if (currentBlip >= blipArray.Count -1)
        {
            currentBlip = 0;
        }
    }

    void DoInput()
    {
       

        

        rotAngle = 0;
        //Vector3 perp = Vector3.Cross(transform.forward, blipArray[currentBlip].rotation);
        //Debug.Log(transform.forward);
        //rotAngle = Mathf.Clamp( Vector3.Dot(perp, transform.up),-1,1);

        if (RM.currentRealSpeed <80 && RM.currentHealth > lowestAcceptableHealth)
        {
            Quaternion oldRot = transform.rotation;
            transform.LookAt(blipArray[currentBlip].position);

            transform.rotation = Quaternion.Lerp(oldRot,transform.rotation, Time.deltaTime * 3);
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(blipArray[currentBlip].rotation), Time.deltaTime * 3);
        }


        // RM.leftRight = rotAngle;

        if (RM.currentRealSpeed < blipArray[currentBlip].speed)
        {
            if (RM.currentHealth > lowestAcceptableHealth)
            { RM.thrust = 1; }
            else { RM.thrust = 0.5f; }
            RM.brake = false;
        }
        else
        if (RM.currentRealSpeed > blipArray[currentBlip].speed )
        { RM.thrust = 0; }

        if (blipArray[currentBlip].braking && RM.currentRealSpeed > blipArray[currentBlip].speed)
        {
            RM.brake = true;
        }

        if (RM.engineTemp < maxAcceptableEngineTemp && RM.currentHealth > (lowestAcceptableHealth * 2 )&& blipArray[currentBlip].boosting)
        { RM.boost = true; }
        else { RM.boost = false; }
    }
}
