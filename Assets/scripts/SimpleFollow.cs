﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFollow : MonoBehaviour {


    public Transform target;
    public bool followHeight = true;
    public bool minimapRotate = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (followHeight)

        { transform.position = target.transform.position; }
        else
        {
            transform.position = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
        }
        if (minimapRotate)
        {
      
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, target.transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
        }
	}
}
