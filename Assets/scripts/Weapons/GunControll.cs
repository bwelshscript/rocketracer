﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunControll : MonoBehaviour {

    public Gun[] guns;
    public bool fireWeapons;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

        if (fireWeapons)
        {
            foreach (Gun gun in guns)
            {
                gun.FireGun(GetComponent<Rigidbody>().velocity);
            }
        }

	}
}
