﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public Transform parent;

    public enum damageType { ballistic, explosive, laser};
    public damageType thisDamageType;

    public float damage;
    public float heatDamage;

    public float baseSpeed;
    public float inheritedSpeedMult = 0;

    // on hit graphics

    public GameObject collisionExplosion;
    public GameObject trailEffect;
    public GameObject firingSound;

	// Use this for initialization
	void Start () {
       

        if (firingSound != null)
        { Instantiate(firingSound,transform.position, transform.rotation, null); }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void AntiTunneling()
    {
        // stretch out collider to compensate for projectile move speed
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform != parent || !collision.transform.IsChildOf(parent))
        {
            GameObject tempObj = Instantiate(collisionExplosion, transform.position, transform.localRotation, null);
            trailEffect.transform.parent = tempObj.transform;


            Destroy(this.gameObject);
        }
    }
}
