﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileLauncher : Gun {

    public  GameObject rocketPrefab;
    public GameObject RacerParent;
    private RocketMovement parentRM;
    public LayerMask ignoredLayers;
    public Collider[] localIgnoredColliders;
    float currentMoveSpeed;
    public float maxDist = 5000;
    Camera mainCam;


	// Use this for initialization
	void Start () {
        mainCam = Camera.main;
        localIgnoredColliders = RacerParent.GetComponentsInChildren<Collider>();
        parentRM = RacerParent.GetComponentInChildren<RocketMovement>();
	}
	
	// Update is called once per frame
	void Update () {



        //if (Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    currentMoveSpeed = parentRM.currentRealSpeed;
        //    MouseclickExplosion();
        //}

        DoTiming();

    }

    public override void FireGun(Vector3 currentVelocity)
    {
        if (canFire)
        {
            RM.engineTemp += firingHeatDamage;
            canFire = false;
            // get diretion or point to fire rocket to, do rocket behaviour except for using mouseclick point to fire.
            currentMoveSpeed = parentRM.currentRealSpeed;

            Vector3 targetPos = Vector3.zero;
            RaycastHit hit;

            if (Physics.Raycast(mainCam.transform.position, mainCam.transform.forward, out hit, maxDist, ignoredLayers))
            {
                targetPos = hit.point;

                GameObject rocket = Instantiate(rocketPrefab, transform.position, transform.rotation);

                rocket.GetComponent<Missile>().InitialiseRocket(transform.position, targetPos, localIgnoredColliders, currentMoveSpeed);
            }
            else
            {
                Debug.Log("Rocket launch found no viable target point");
            }

        }
    }

     void  MouseclickExplosion()
    {
        Vector3 targetPos = Vector3.zero;
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, ignoredLayers))

            targetPos = hit.point;

        GameObject rocket = Instantiate(rocketPrefab, transform.position , transform.rotation);

        rocket.GetComponent<Missile>().InitialiseRocket(transform.position, targetPos, localIgnoredColliders, currentMoveSpeed);

    }
}
