﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//this is the gun base script, all weapon types should derive from this.
//

public class Gun : MonoBehaviour {

    public RocketMovement RM;
    public float accuracy;
    public float fireRate;
    float fireRateTimer;
    public bool canFire = false;

    public bool holdingTrigger = false;

    public Projectile bullet;
    public Vector3 firingOffset;

    public float firingHeatDamage;

    // Use this for initialization
    void Start () {
        fireRateTimer = fireRate;
	}
	
	// Update is called once per frame
	void Update () {

        DoTiming();
	}

    public void DoTiming()
    {
        fireRateTimer -= Time.deltaTime;
        if (fireRateTimer <= 0)
        {
            canFire = true;
            fireRateTimer = fireRate;
        }

    }

    public virtual void FireGun( Vector3 currentVelocity )
    {
        if (canFire)
        {
            RM.engineTemp += firingHeatDamage;
            canFire = false;
            Debug.Log("firing");

            Projectile tempBullet =
            Instantiate(bullet, transform.position + firingOffset, transform.rotation, null).GetComponent<Projectile>();
            tempBullet.parent = transform.parent.parent;
            tempBullet.GetComponent<Rigidbody>().AddForce((currentVelocity*tempBullet.inheritedSpeedMult) + (tempBullet.baseSpeed * transform.forward), ForceMode.Impulse);
        }
    }
}
