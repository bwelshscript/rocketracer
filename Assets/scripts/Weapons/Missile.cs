﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

    public float explosionRadius = 10;
    public int maxDamage = 25;

    public float startMoveSpeed = 0.5f;
    public float accelerationMult = 1;
    public float topMoveSpeed = 1;
    private float currentMoveSpeed;
    public float startCurveMult;

    public Vector3 curveHeight;
    public GameObject explosionSFX;

    private Vector3 startPoint;
    private Vector3 endPoint;

    [Range(0,1)]
    private float rocketTime = 0;
    private float targetDistance;
    private bool hasExploded = false;

   public  GameObject[] switchOffOnCollide;
    private Vector3 lastPos = Vector3.zero;
	
    public Collider[] ignoredColliders;

    void Awake()
    {
        lastPos = transform.position - transform.forward;
    }
	// Update is called once per frame
	void Update () {

        if (!hasExploded)
        {
            rocketTime += Time.fixedDeltaTime;
            transform.position = CalculateBezierPoint(CalculatePathTime(rocketTime), startPoint, startPoint + (curveHeight * startCurveMult), endPoint + curveHeight, endPoint);

            transform.LookAt(transform.position + ((transform.position - lastPos) * 3));
            lastPos = transform.position;
        }

	}

    public void OnTriggerEnter(Collider other)
    {


        if (other.tag != "Rocket" && other.tag != "Checkpoint")
        {
            bool selfCol = false;
            foreach (Collider col in ignoredColliders)
            {
                if (other == col) { selfCol = true; }
            }

            if ( !selfCol)
            {
                Debug.Log("collided with " + other.name);
                RocketExplode();
            }
    
        }
    }

    public void RocketExplode()
    {
        if (hasExploded) { return; }
        else { hasExploded = true; }

        foreach (GameObject item in switchOffOnCollide)
        {
            item.SetActive(false);
        }

        //send damage info somewhere
        ApplyDamage();

        // create explosion effect on impact
        Instantiate(explosionSFX, transform.position, transform.rotation, null);

        // create optional physics, like a wind system burst and explosion force
        // spherecast around this, add damage to anything that can take damage, add physics to appropriate physics bodies


       // GetComponent<MeshRenderer>().enabled = false;
        //DestroySelf();
    }

    void ApplyDamage()
    {
        var cols = Physics.OverlapSphere(transform.position, explosionRadius);
        var rigidbodies = new List<Rigidbody>();

        foreach (var col in cols)
        {
            if (col.attachedRigidbody != null && !rigidbodies.Contains(col.attachedRigidbody) && col.tag == "Racer")
            {
                rigidbodies.Add(col.attachedRigidbody);
                //TODO: set up damage ramp based on distance to explosion.
                col.GetComponent<RocketMovement>().ReceiveDamage(maxDamage);
            }
        }
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(2 * targetDistance);
        Destroy(gameObject);
    }

    public void InitialiseRocket (Vector3 startPos, Vector3 endPos, Collider[] collidersToIgnore, float currSpeed)
    {
        ignoredColliders = collidersToIgnore;
        startPoint = startPos;
        endPoint = endPos;

      //  if (endPos.y > startPoint.y) { curveHeight = -curveHeight; }
        //get distance to target, multiply curve height by a fraction of that distance (further targets aim higher)

        targetDistance = Vector3.Distance(startPoint, endPoint);

        curveHeight *= targetDistance * 0.02f;

        topMoveSpeed += currSpeed;
        startMoveSpeed = currSpeed + (currSpeed *0.8f);
    }

    private float CalculatePathTime (float time)
    {
        // accelerate up to this value
        currentMoveSpeed = Mathf.Lerp(startMoveSpeed, topMoveSpeed, Time.deltaTime * accelerationMult);
        float movementPerFrame =  targetDistance / currentMoveSpeed;
        time = (time  / movementPerFrame) ;


        // normalise distance to know where we are meant to be along the path

       // Mathf.Clamp01(time);
          if (time >= 5f) { RocketExplode(); Debug.Log("timer end explosion"); }
       // Debug.Log(time);
     
        return time;
    }

    private Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0; //first term
        p += 3 * uu * t * p1; //second term
        p += 3 * u * tt * p2; //third term
        p += ttt * p3; //fourth term

        return p;
    }


}
