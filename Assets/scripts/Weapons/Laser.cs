﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Gun {

    public ParticleSystem ps;
    // List<GameObject> laserShotPool = new List<GameObject>();
     GunControll GC;
    public float width;
    public float maxRange = 500;
    public LayerMask collisionLayers;
    public enum damageType { ballistic, explosive, laser };
    public damageType thisDamageType;
    public int damage;
   
    bool displayLaser = false;

    public GameObject laserDamageGraphics;

    ParticleSystem.MainModule newmm;
    ParticleSystem.ShapeModule newsh;

    // Use this for initialization
    void Start () {

        GC = RM.transform.GetComponent<GunControll>();
        PSInit();
        
    }

    void PSInit()
    {
        newmm = ps.main;
        newsh = ps.shape;

        newmm.startSizeX = width;
        newmm.startSizeY = width;
        newmm.startSizeZ = maxRange;

       
    }

    void PSSetDist()
    {
        float dist = maxRange;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxRange))
        {
            dist = Vector3.Distance(transform.position, hit.point);
        }

       // newmm = ps.main;
      //  newsh = ps.shape;

        newmm.startSizeZ = dist;
        newsh.position = new Vector3(0, 0, (dist * 0.5f) );
        
    }

    // Update is called once per frame
    void Update () {

        DoTiming();
        PSSetDist();
      //  if (canFire) { displayLaser = false; }

        if (GC.fireWeapons) { canFire = true; } else { canFire = false; displayLaser = false; }
        ps.loop = displayLaser;
       


        if (!ps.isPlaying && displayLaser)
        {
            ps.Play();
        
        }

	}

    //GameObject AddToPool()
    //{
    //    GameObject newShot = Instantiate(laserShotPrefab);
    //    laserShotPool.Add(newShot);
    //    return newShot;
    //}

    //void DisplayFreshLaserShot(  )
    //{
    //    GameObject freshShot = null;
    //    ParticleSystem ps = null ;
    //    //get used laser from pool
    //    foreach (GameObject laser in laserShotPool)
    //    {
    //       ps  = laser.GetComponent<ParticleSystem>();

    //        if (!ps.IsAlive())
    //        {
    //            freshShot = laser;
    //            break;
    //        }
    //    }

    //    //else make new laser shot
    //    if (freshShot == null)
    //    {
    //        freshShot = AddToPool();
    //        ps = freshShot.GetComponent<ParticleSystem>();
    //    }


    //    //reset shot
    //    ps.Clear();

    //    ParticleSystem.MainModule newmm = ps.main;

    //    ps.transform.position = transform.position;
    //    ps.transform.rotation = transform.rotation;
    //    newmm.startSizeX = width;
    //    newmm.startSizeY = width;
    //    newmm.startSizeZ = maxRange;

    //   // ps.set = newmm;

    //    ps.Play();

       

    //}

    public override void FireGun(Vector3 currentVelocity)
    {
        if (canFire)
        {
            RM.engineTemp += firingHeatDamage;
            canFire = false;
            RaycastHit hit;

            // display laser graphic and sound whether we hit something or not. 
            displayLaser = true;
            


            // spherecast forward from gun to range distance // apply width to collision sphere
            if (Physics.SphereCast(transform.position, width * 0.25f, transform.forward, out hit, maxRange, collisionLayers))
            {
                Debug.DrawLine(transform.position, hit.point, Color.magenta);

                // apply heat and damage to collided target

                DealDamage(hit.transform.gameObject);

                // apply width to graphic scale

                // display collision graphics
                CreateLazerFires(hit.point, hit.transform);
            }
            else
            {
              
            }
        }
    }

   void DealDamage(GameObject hitGO)
    {
        if (hitGO.tag == "Racer")
        {
            RocketMovement hitRM = hitGO.GetComponent<RocketMovement>();

            if (hitRM == null)

            { hitRM = hitGO.transform.parent.transform.parent.GetComponentInChildren<RocketMovement>(); }

            hitRM.ReceiveDamage(damage);
            hitRM.ReceiveHeatDamage((int)(firingHeatDamage * 1.75f));
        }
    }
    // fire lazer at all times trigger is pulled

    void CreateLazerFires(Vector3 position, Transform parent)
    {
        Instantiate(laserDamageGraphics, position, transform.rotation, parent);
    }
}
