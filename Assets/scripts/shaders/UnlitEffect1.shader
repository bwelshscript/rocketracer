﻿Shader "Unlit/UnlitEffect1"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
//	Shader Inputs
	//	uniform vec3      iResolution;           // viewport resolution (in pixels)
//	uniform float     iTime;                 // shader playback time (in seconds)
	//uniform float     iTimeDelta;            // render time (in seconds)
	//uniform int       iFrame;                // shader playback frame
	//uniform float     iChannelTime[4];       // channel playback time (in seconds)
	//uniform float2      iChannelResolution[4]; // channel resolution (in pixels)
	//uniform float4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
	//uniform samplerXX iChannel0..3;          // input channel. XX = 2D/Cube
	//uniform float4      iDate;                 // (year, month, day, time in seconds)
	//uniform float     iSampleRate;           // sound sample rate (i.e., 44100)

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			static const float PI = 3.14159265f;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			float ringStep(float outerRadius, float innerRadius, float2 coord, float rotationOffset) {
				float d = length(coord);
				const float smoothingWidth = 0.02;
				float ringValue = smoothstep(smoothingWidth, 0.0, d - outerRadius) * smoothstep(0.0, smoothingWidth, d - innerRadius * 2);
			//	float radialMultiplier = fract(atan2(coord.x, coord.y) * 7.0 / 6.28 - 0.3 * _Time.y + rotationOffset);
				float radialMultiplier = frac(atan2(coord.x, coord.y) * 7.0 / 6.28 - 0.3 * _Time.y + rotationOffset);
				return ringValue * smoothstep(0.0, 0.1, radialMultiplier - 0.4);
			}

			float noiseish(float2 coord, float2 coordMultiplier1, float2 coordMultiplier2, float2 coordMultiplier3, float3 timeMultipliers) {
				return 0.5 + 0.1667 * (sin(dot(coordMultiplier1, coord) + timeMultipliers.x * _Time.y) 
					+ sin(dot(coordMultiplier2, coord) + timeMultipliers.y * _Time.y) 
					+ sin(dot(coordMultiplier3, coord) + timeMultipliers.z * _Time.y));
			}

			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);

			float2 uv =i.uv / _ScreenParams.y;
			uv.x += 0.04 *  cos((0.6 * uv.x + 0.7 * uv.y - 4.0*cos(0.3 * _Time.y) * (0.3 + 0.02*uv.y)));
			const float cellResolution = 7.0;
			float2 localUV = frac(uv * cellResolution) - float2(0.5,0.5);
			const float smoothingWidth = 0.04;

			float2 cellCoord = floor(uv * cellResolution);

			float cellValue = noiseish(cellCoord, float2(1.3, -1.0), float2(1.7, 1.9), float2(0.3, 0.7), float3(-1.3, 2.3, -0.8));
			float outer1 = 0.2 + 0.3 * cellValue;
			float inner1 = 0.02 + 0.38 * pow(cellValue, 0.8);

			float cellValue2 = noiseish(cellCoord, float2(-2.3, 1.1), float2(2, 1), float2(1,2), float3(2.1, 1.9, -1.7));

			float v = 1.0 - (ringStep(outer1, inner1, localUV, 0.0)
				+ ringStep(0.05 + 0.25 * cellValue2, 0.05 + 0.05 * cellValue2, localUV, _Time.y * 1.2));
			float4 fragColor = float4(0.1, 0.1, 0.1, 0.1);
			if (v != 1.0)
			{
				fragColor = float4(v - cos(_Time.x ), v + cos(PI/3+_Time.y), v*v + cos( (2*PI) /3+_Time.z), 1.0);
			}
				return fragColor;
			}
			ENDCG
		}
	}
}
