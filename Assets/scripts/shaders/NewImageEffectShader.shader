﻿Shader "Hidden/NewImageEffectShader"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_HeightMin("Height Min", Float) = 20
		_HeightMax("Height Max", Float) = 100
		_ColorMin("Tint Color At Min", Color) = (0, 0, 0, 1)
		_ColorMax("Tint Color At Max", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
// Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members uv_MainTex,worldPos)
#pragma exclude_renderers d3d11
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"



			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD1;
				float2 uv_MainTex;
				float3 worldPos;
				
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = v.vertex;		
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 _ColorMin;
			fixed4 _ColorMax;
			float _HeightMin;
			float _HeightMax;

			fixed4 frag (v2f i) : SV_Target
			{
				
				float h = (_HeightMax - i.vertex.y) / (_HeightMax - _HeightMin);
				fixed4 tintColor = lerp(_ColorMax.rgba, _ColorMin.rgba, h);				
				return tintColor;
			}
			ENDCG
		}
	}
}
