﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

    public RocketMovement RM;
    public CamFollow mainCamFollow;
    public UnityStandardAssets.ImageEffects.Fisheye FE;

    public float FOV = 50;
    public float FEstr = 0;
    public Transform camRotator;
    public Quaternion startCamRotation;
    int camResetTimer = 360;

    // Use this for initialization
    void Start () {

        Application.targetFrameRate = 60;
        startCamRotation = Quaternion.identity;
    }
	
	// Update is called once per frame
	void Update () {

      

        if (!RM.alive)
        {
            // MOVE TO GAME CONTROLLER

            if (Input.GetButtonDown("Submit")) { SceneManager.LoadScene(0); }
        }

        if (RM.canMove)
        {
            if (!RM.breaking && RM.boost && RM.canBoost)
            {
                if (RM.speed > RM.startMaxSpeed)
                {
                    FOV = Mathf.Lerp(FOV, 120, Time.deltaTime * 0.5f);
                    FEstr = Mathf.Lerp(FEstr, 0.6f, Time.deltaTime * 0.5f);
                    if (FOV < 110) { mainCamFollow.ToggleCloseTarget(1.5f); }
                }
                else
                {
                    FOV = Mathf.Lerp(FOV,100, Time.deltaTime * 4);
                    FEstr = Mathf.Lerp(FEstr, 0.2f, Time.deltaTime *4);
                    if (FOV > 90) { mainCamFollow.ToggleCloseTarget(0.75f); }
                }
            }
            else
            {
                FOV = Mathf.Lerp(FOV, 60, Time.deltaTime);
                FEstr = Mathf.Lerp(FEstr, 0, Time.deltaTime);
                if (FOV > 60) { mainCamFollow.ToggleCloseTarget(0); }
            }
        }
        RM.mainCam.fieldOfView = FOV;
        FE.strengthX = FEstr;
        FE.strengthY = FEstr * 0.25f;
    }

    private void FixedUpdate()
    {
        PlayerCamControl();
    }

    void PlayerCamControl()
    {
        //MOVE TO CAMERA CONTROLLER SCRIPT
        float camHorizontal = -Input.GetAxis("HorizontalArrows") * 2.5f;
        float camVertical = Input.GetAxis("VerticalArrows") * 2.5f;

        if (Mathf.Abs(Input.GetAxis("HorizontalArrows")) + Mathf.Abs(Input.GetAxis("VerticalArrows")) == 0)
        {
            if (camResetTimer > 0) { camResetTimer -= 1; }
            else { camRotator.rotation = Quaternion.Lerp(camRotator.rotation, Quaternion.Euler(transform.rotation.eulerAngles + startCamRotation.eulerAngles), Time.deltaTime * 2); }
        }
        else { camRotator.rotation = Quaternion.Euler(new Vector3(camVertical, camHorizontal, 0) + camRotator.rotation.eulerAngles); camResetTimer = 10; }
    }
}
