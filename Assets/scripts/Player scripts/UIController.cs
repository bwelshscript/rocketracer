﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIController : MonoBehaviour {

    public Text speedUI;
    public Text hpUI;
    public Text engineTempUI;
    public Text resetUI;
    public RocketMovement RM;
    public RaceController RC;
    public Text AIVisibleUI;
    public Camera mainCam;
    RectTransform rectTrans;
    Text[] AIVisibleUIArray;
    float[] visibleTimersArray;
    float currentHeat = 0;
    bool lastPausedState = false;

    bool updatedOnceSinceDead = false;
    // Use this for initialization
    void Start () {
        RC = FindObjectOfType<RaceController>();
        AIVisibleUIArray = new Text[RC.racers.Length];
        visibleTimersArray = new float[AIVisibleUIArray.Length];
        AIVisibleUIArray[0] = AIVisibleUI;
        mainCam = Camera.main;
        rectTrans = speedUI.transform.parent.GetComponent<Canvas>().GetComponent<RectTransform>();
        for (int i = 1; i < AIVisibleUIArray.Length; i++)
        {
            AIVisibleUIArray[i] = Instantiate(AIVisibleUI, AIVisibleUI.transform.parent);
            
        }

        Debug.Log(AIVisibleUIArray.Length);
	}
	
	// Update is called once per frame
	void Update () {

        if (RM.alive) { UpdateUI(); }
        else {
            resetUI.enabled = true;
            if (!updatedOnceSinceDead)
            { UpdateUI(); updatedOnceSinceDead = true; }
        }

        UpdateAIVisPosUI();

        if (RC.isPaused != lastPausedState)
        {
            PauseToggleBehaviour();
        }
    }
    bool isInFront(GameObject obj)
    {
        if (Vector3.Dot(transform.forward, transform.InverseTransformPoint(obj.transform.position)) < 0 
            )

        { return true; }
        else { return false; }
    }

    void PauseToggleBehaviour()
    {
        lastPausedState = RC.isPaused;
        resetUI.enabled = lastPausedState;
        if (lastPausedState) { resetUI.text = "PAUSED"; }
        else { resetUI.text = "PRESS ENTER/START TO RESET"; }
    }

    void UpdateAIVisPosUI()
    {

        for (int i = 1; i < AIVisibleUIArray.Length; i++)
        {
            RaycastHit hit;
            visibleTimersArray[i] -= Time.deltaTime * 3;
            //if (isInFront(RC.racers[i]))
            //{
            if (Physics.Raycast(RM.mainCam.transform.position, RC.racers[i].transform.position - RM.mainCam.transform.position, out hit, 5000))
            {

                if (hit.transform.gameObject == RC.racers[i].transform.gameObject)
                {
                    // AIVisibleUIArray[i].enabled = true;
                    visibleTimersArray[i] += Time.deltaTime * 6;

                    AIVisibleUIArray[i].text = RC.racerPlacementArray[i].ToString();

                    Vector2 ViewportPosition = mainCam.WorldToViewportPoint(RC.racers[i].transform.position);
                    Vector2 WorldObject_ScreenPosition = new Vector2(
                    ((ViewportPosition.x * rectTrans.sizeDelta.x) - (rectTrans.sizeDelta.x * 1)),
                    ((ViewportPosition.y * rectTrans.sizeDelta.y) - (rectTrans.sizeDelta.y * 0.5f)));

                    AIVisibleUIArray[i].rectTransform.anchoredPosition = WorldObject_ScreenPosition;
                    // AIVisibleUIArray[i].rectTransform.anchoredPosition = ViewportPosition;
                }


                //  }

            }

            visibleTimersArray[i] = Mathf.Clamp01(visibleTimersArray[i]);
            AIVisibleUIArray[i].color = new Color(1, 1, 1, visibleTimersArray[i]);
        }

    }

    void UpdateUI()
    {
        speedUI.text = (RM.currentRealSpeed).ToString("00") + "KM/H";
        hpUI.text = (RM.currentHealth).ToString("0") + "%";
        currentHeat = Mathf.Lerp(currentHeat, RM.engineTemp, Time.deltaTime * 3);
        engineTempUI.text =(currentHeat).ToString("0") + "ºC";


        engineTempUI.color = Color.Lerp(Color.white, Color.red, ((RM.engineTemp - 160) / (RM.tempMaxThresh)));
        hpUI.color = Color.Lerp(Color.red, Color.white, RM.currentHealth / 100);



    }
}
