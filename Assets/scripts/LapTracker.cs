﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class LapTracker : MonoBehaviour {

    public bool trackLap = false;
    public bool placeBlips = false;
    public GameObject minimapBlipPrefab;
    public GameObject blipParent;

   public  List<BlipInfo> mapBlipPositions = new List<BlipInfo>();
  public   List<BlipInfo> oldMapBlipPositions = new List<BlipInfo>();
    public float blipBetweenTime;
    public float placementTimer;
    public bool startedLap;
    public bool finishedLap;
  
    public string gameDataFileName = "file.json";
   public  RaceController RC;
    public RocketMovement RM;



    // Use this for initialization
    void Start () {

        
        gameDataFileName = "Resources/" + SceneManager.GetActiveScene().name + ".json";

        InputLap();
        RC = FindObjectOfType<RaceController>().GetComponent<RaceController>();
        RM = GetComponent<RocketMovement>();
    }
	
	// Update is called once per frame
	void Update () {
		
        if (trackLap && startedLap)
        {
          
            TrackNewLap();
        }

        if (placeBlips)
        {
            PlaceMapBlips();
            placeBlips = false;
        }
      
	}

    void ClearAllBlips()
    {
        oldMapBlipPositions.Clear();
        mapBlipPositions.Clear();
        foreach (Transform item in blipParent.GetComponentInChildren<Transform>())
        {
            Destroy(item.gameObject);
        }
    }

    BlipInfo BuildBlip()
    {
      
        BlipInfo newBlip = new BlipInfo();
        newBlip.position = transform.position;
        newBlip.rotation = transform.rotation.eulerAngles;
        newBlip.speed = RM.currentRealSpeed;
        newBlip.boosting = RM.boost;
        newBlip.braking = RM.brake;
        return newBlip;
    }

    void TrackNewLap()
    {
        if (finishedLap)
        {

         
            mapBlipPositions.Add(BuildBlip());
            finishedLap = false;
            startedLap = false;
      

            OutputLap();
            ClearAllBlips();
            InputLap();
           // PlaceMapBlips();

            return;
        }

        if (placementTimer <=0)
        {
           
            mapBlipPositions.Add(BuildBlip());
            placementTimer = blipBetweenTime;
        }
        else
        {
            placementTimer -= Time.deltaTime;
        }
    }

    void OutputLap()
    {
        File.WriteAllText(@gameDataFileName, string.Empty);

        using (StreamWriter r = new StreamWriter(gameDataFileName))
        {
           

            foreach (BlipInfo item in mapBlipPositions)
            {



                // string json = r.ReadToEnd();
                //  mapBlipPositions = JsonConvert.DeserializeObject<List<Vector3>>(json);

                r.Write(Mathf.Round(item.rotation.x));
                r.Write(",");
                r.Write(Mathf.Round(item.rotation.y));
                r.Write(",");
                r.Write(Mathf.Round(item.rotation.z));
                r.Write(":");

                r.Write(Mathf.Round(item.position.x));
                r.Write(",");
                r.Write(Mathf.Round(item.position.y));
                r.Write(",");
                r.Write(Mathf.Round(item.position.z));
                r.Write(":");

                r.Write((int)item.speed);
                r.Write(":");

                r.Write(item.braking);
                r.Write(":");

                r.Write(item.boosting);


                r.Write("\n");


            }
        }
    }

    void InputLap()
    {
        // Path.Combine combines strings into a file path
        // Application.StreamingAssets points to Assets/StreamingAssets in the Editor, and the StreamingAssets folder in a build
        string filePath = gameDataFileName;
        Vector3 tempRot = Vector3.zero;
        Vector3 tempPos = Vector3.zero;
        float tempSpeed = 0;
        bool tempBrake = false;
        bool tempBoost = false;


        if (File.Exists(filePath))
        {
            // Read the json from the file into a string
            using (StreamReader r = new StreamReader(gameDataFileName))
            {
                string json = r.ReadToEnd();
                string[] splitJson = json.Split(new string[] { "\n" }, System.StringSplitOptions.None);

                foreach (string blipInfos in splitJson)
                {
                    string[] variables = blipInfos.Split(new string[] { ":" }, System.StringSplitOptions.None);

                    // 1
                    string[] rotation = variables[0].Split(new string[] { "," }, System.StringSplitOptions.None);
                    if (variables.Length > 4)
                    {
                        if (rotation.Length == 3)
                        {
                            float.TryParse(rotation[0], out tempRot.x);
                            float.TryParse(rotation[1], out tempRot.y);
                            float.TryParse(rotation[2], out tempRot.z);
                        }

                        // 2
                        string[] position = variables[1].Split(new string[] { "," }, System.StringSplitOptions.None);

                        if (position.Length == 3)
                        {
                            float.TryParse(position[0], out tempPos.x);
                            float.TryParse(position[1], out tempPos.y);
                            float.TryParse(position[2], out tempPos.z);
                        }

                        // 3
                        string speed = variables[2];
                        float.TryParse(speed, out tempSpeed);
                        // 4
                        if (variables[3] == "True") tempBrake = true;
                        // 5
                        if (variables[4] == "True") tempBoost = true;


                        //add info into new blip
                        BlipInfo newBlip = new BlipInfo();
                        newBlip.position = tempPos;
                        newBlip.rotation = tempRot;
                        newBlip.speed = tempSpeed;
                        newBlip.boosting = tempBoost;
                        newBlip.braking = tempBrake;

                        oldMapBlipPositions.Add(newBlip);
                    }
                }


            }



        }
        else
        {
            Debug.LogError("Cannot load game data! file path is: " + filePath);
        }

        PlaceMapBlips();
    }

   

    void PlaceMapBlips()
    {
        // read in array at map location,
        //for each Vector3, place a blip.

      //  Debug.Log("placedBlip");

        foreach (BlipInfo oldBlip in oldMapBlipPositions)
        {
          Instantiate(minimapBlipPrefab, oldBlip.position, minimapBlipPrefab.transform.rotation, blipParent.transform);
        }

        RC.SendBlips(oldMapBlipPositions.ToArray());
    }
}
