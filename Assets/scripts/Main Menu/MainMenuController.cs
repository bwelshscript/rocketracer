﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController instance = null;

    public Animator transitionHolder;
    public Animator titleHolder;
    public Animator bannerController;
    public Animator mapZoom;

    public GameObject titleBackground;

    public Color locationDefault = Color.white;
    public Color locationHighlighted = Color.white;

    public Image hatchB;
    public Image hatchT;

    // Use this for initialization
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SetAnimatorState(titleHolder, "Transition", 1);         //Start Title move off screen
            SetAnimatorState(transitionHolder, "Transition", 2);    //Start Transition Close.
            StartCoroutine(StartDelay());
        }
        UpdateHatchTexture();
    }

    public void SetAnimatorState(Animator _animator, string _string, int _int)
    {
        _animator.SetInteger(_string, _int);
    }

    private IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(1);
        SetAnimatorState(bannerController, "Transition", 1);
        yield return new WaitForSeconds(1);
        titleBackground.SetActive(false);
    }

    void UpdateHatchTexture()
    {
        float temp = 1- GetNormalizedRectValue(hatchT.rectTransform);
        hatchT.material.SetTextureScale("_MainTex", new Vector2(2.5f, temp));
        hatchB.material.SetTextureScale("_MainTex", new Vector2(2.5f, temp));
    }

    float GetNormalizedRectValue(RectTransform rect)
    {
        float temp = (rect.offsetMin.y - 540) / (1080 - 540);
        Debug.Log(temp);
        return  temp;

    }

    public void MapZoomAnimation(int _state)
    {
        mapZoom.SetInteger("Zoom", _state);
    }

    public void LocationHighLight(Image _image)
    {
        _image.color = locationHighlighted;
    }

    public void LocationDeHighlight(Image _image)
    {
        _image.color = locationDefault;
    }
}
