﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionHolder : MonoBehaviour
{
    public Animator myAnimator;

    public void ResetTransition()
    {
        myAnimator.SetInteger("Transition", 0);
    }
}
