﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapNavigation : MonoBehaviour
{
    public RectTransform map;
    public RectTransform garage;
    public RectTransform marketDistrict;
    public RectTransform tournamentOffice;
    public RectTransform quit;
    public RectTransform mapPosition;

    private float timer = 0f;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(timer <= 1)
        {
            timer += Time.deltaTime;
            LerpMapDestination(mapPosition);
            
            if(timer >= 0.6f)
            {
                MainMenuController.instance.MapZoomAnimation(1);
            }
        }
    }

    private void LerpMapDestination(RectTransform _rt)
    {
        map.anchoredPosition =  Vector2.Lerp(map.anchoredPosition, _rt.anchoredPosition, timer);
    }

    public void SetMapDestination(RectTransform _destination)
    {
        ResetTimer();
        mapPosition = _destination;
    }

    private void ResetTimer()
    {
        timer = 0f;
        MainMenuController.instance.MapZoomAnimation(0);
    }
}
