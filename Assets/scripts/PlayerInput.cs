﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {

    public RocketMovement RM;
    public GunControll GC;
    public bool DebugMode = false;
    bool slowedTime = false;
  

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        RM.leftRight = Input.GetAxis("Horizontal");
        RM.thrust = Input.GetAxis("Vertical");
        RM.boost = Input.GetButton("Fire3");
        RM.brake = Input.GetButton("Fire2");

        if (DebugMode && Input.GetButtonDown("Submit"))
        {
            if (!slowedTime) { slowedTime = !slowedTime; Time.timeScale = 0.3f; }
            else { slowedTime = !slowedTime; Time.timeScale = 1f; }
        }

        GC.fireWeapons = Input.GetButton("Fire1");

	}


}
