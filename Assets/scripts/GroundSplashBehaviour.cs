﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSplashBehaviour : MonoBehaviour {
    public Color currentColour;
     Material myMat;

	// Use this for initialization
	void Start () {

        myMat = GetComponent<Renderer>().material;

	}
	
	// Update is called once per frame
	void Update ()

    {
        myMat.SetColor("_Color", currentColour);
	}
}
