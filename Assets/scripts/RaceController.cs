﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class RaceController : MonoBehaviour {

  public BoxCollider[] checkPoints;
  public BoxCollider startLine;
  public BoxCollider finishLine;
    

    public GameObject[] racers;
    public RocketMovement player;
   public int[] racerCheckpointindex;
    int[] racerCurrentLapAmount;
    //[HideInInspector]
    public int[] racerPlacementArray;
   // int[] CheckpointsPassedIndex ; //the higher this number the better
   public float[] DistanceToNextCheckpoint ; //the lower this is the better
   public int[] tempIntArray;
     int[] lapTimer;

    public int playerInFirst = 0;

    Text[] placementUIArray;
    Vector3[] placementUIPositions;

    float miliseconds =0;
    int seconds =0;
    int minutes =0;
    public Text lapTimerUI;
    bool startTimer = false;
    bool hasBegun = false;

    int personalbesttime = 999999;
    int globalbesttime = 999999;

    public Text pbScoreUI;
    public Text gbScoreUI;
    public Text placementUI;

    public LapTracker laptrack;
    private int placementUpdateFrequency = 10;

    bool raceHasStarted = false;
    bool hasStartedRaceTimers = false;
    bool completedUIInit = true;
    bool createdPlacementUIElements = false;
    Vector3 placementUIStartPos;
     float UIInitTimer = 0;

    public int lapsToFinish = 3;
    private float timeUntilStart = 2;

    public bool isPaused = false;

	// Use this for initialization
	void Start () {

        isPaused = false;

        racerCheckpointindex = new int[racers.Length];
        foreach (BoxCollider item in checkPoints)
        {
            item.gameObject.AddComponent<Checkpoint>();

           

           personalbesttime =  PlayerPrefs.GetInt("PersonalBestTime");
            if (personalbesttime < 5) { personalbesttime = 999999; PlayerPrefs.SetFloat("PersonalBestTime", personalbesttime); }

        }

        UpdateScoreUI();
        racerPlacementArray = new int[racers.Length];
        racerCurrentLapAmount = new int[racers.Length];
        tempIntArray = new int[racers.Length];
        placementUIArray = new Text[racers.Length];
        placementUIPositions = new Vector3[racers.Length];

        for (int i = 0; i < racers.Length; i++)
        {
           placementUIPositions[i] =  placementUI.rectTransform.position;
        }

        lapTimer = new int[racers.Length];
        DistanceToNextCheckpoint = new float[racers.Length];
        placementUIStartPos = placementUI.rectTransform.position;
    }
	
	// Update is called once per frame
	void Update () {

        if (timeUntilStart > 0)
        { timeUntilStart -= Time.deltaTime; }

        if ( timeUntilStart <=0 && !raceHasStarted && Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f) { StartRace(); }

            if (startTimer && player.alive && !isPaused)
        {

          //  Debug.Log("STARTED TIMER");
            UpdateTimer();

            if (placementUpdateFrequency > 0) { placementUpdateFrequency--; }
            else
            { TrackRacerPlacements(); placementUpdateFrequency = 2; }
        }

        if (!completedUIInit)
        { InitiatePlacementUI(); }
        else if (startTimer)
        {
            UpdatePlacementUI();
        }

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetButtonDown ("Submit"))
        {           
            PauseBehaviour();           
        }
    }

    void InitiatePlacementUI( )
    {
        if (!createdPlacementUIElements)
        {
            placementUIArray[0] = placementUI;

            for (int i = 1; i < racers.Length; i++)
            {
               placementUIArray[i] = Instantiate(placementUI, placementUI.transform.position, placementUI.transform.rotation, placementUI.transform.parent);
            }

            createdPlacementUIElements = true;
        }

        for (int i = 0; i < racers.Length; i++)
        {
            if (UIInitTimer > 1)
            {
                UIInitTimer = 1;
            }
                placementUIArray[i].rectTransform.position =
                placementUIStartPos - new Vector3(0, Mathf.Lerp(0, (racerPlacementArray[i] * 22),UIInitTimer)  , 0);
        }

        if (UIInitTimer == 1)
        {
             completedUIInit = true; 
            UIInitTimer = 0;
            StoreNewPositions();
            return;
        }
            UIInitTimer += Time.deltaTime;
        
    }

    void PauseBehaviour()
    {
        isPaused = !isPaused;

        // show paused text

        Time.timeScale = Convert.ToSingle(!isPaused);
    }

    void UpdatePlacementUI()
    {
        // we update positions every second, 
        // so we can also update the position UI every second too


        for (int i = 0; i < racers.Length; i++)
        {
            if (UIInitTimer > 1)
            {
                UIInitTimer = 1;
            }

            placementUIArray[i].rectTransform.position =
        
             Vector3.Lerp (placementUIArray[i].rectTransform.position, 
             placementUIStartPos - new Vector3(0,racerPlacementArray[i] * 22, 0),  Time.deltaTime * 5);

            placementUIArray[i].text = racerPlacementArray[i].ToString() + "  " + racers[i].gameObject.name.Substring(0,8);

        }
        if (UIInitTimer == 0.1f)
        {
            // completedUIInit = true; 
            UIInitTimer = 0;
            StoreNewPositions();
            return;
        }
        UIInitTimer += Time.deltaTime;
    }


    void StoreNewPositions()
    {
        for (int i = 0; i < racers.Length; i++)
        {
            placementUIPositions[i] = placementUIArray[i].rectTransform.position;
        }
    }

    public void SendBlips(BlipInfo[] blipArray)
    {

        foreach (GameObject item in racers)
        {
            AIInputController tempAI = item.GetComponent<AIInputController>();
            if (tempAI != null) { tempAI.ReceiveBlips(blipArray); }
        }
    }

    private void StartRace()
    {
          //startTimer = true;
      //  StartRaceTimers();
        raceHasStarted = true;
        completedUIInit = false;

        foreach (GameObject item in racers)
        {
            item.GetComponent<RocketMovement>().canMove = true;
        }

      
    }

    private void UpdateScoreUI() //dont do this mid race
    {
        miliseconds = personalbesttime;
        do
        {
            if (miliseconds >= 60) { seconds += 1; miliseconds -= 60; }
            if (seconds >= 60) { minutes += 1; seconds -= 60; }
        }
        while (miliseconds > 60);

            minutes = Mathf.Clamp(minutes, 0, 999);

        pbScoreUI.text = (minutes).ToString("00") + ":" + (seconds).ToString("00") + ":" + (miliseconds).ToString("00");


        miliseconds = 0;
        seconds = 0;
        minutes = 0;
        miliseconds = globalbesttime;
        do
        {
            if (miliseconds >= 60) { seconds += 1; miliseconds -= 60; }
            if (seconds >= 60) { minutes += 1; seconds -= 60; }
        }
        while (miliseconds > 60);

            minutes = Mathf.Clamp(minutes, 0, 999);

        gbScoreUI.text = (minutes).ToString("00") + ":" + (seconds).ToString("00") + ":" + (miliseconds).ToString("00");

      
        miliseconds = 0;
        seconds = 0;
        minutes = 0;
    }

    private void StartRaceTimers()
    {
        for (int i = 0; i < lapTimer.Length; i++)
        {
            lapTimer[i] = 0;
        }
        
        miliseconds = 0;
        seconds = 0;
        minutes = 0;
        startTimer = true;

        laptrack.startedLap = true;

    }

    public void CheckpointUpdate(Collider other, BoxCollider checkpoint)
    {

        // Debug.Log(other.gameObject.name);


        if (checkpoint == startLine && other.transform.parent.tag == "Player" && !hasStartedRaceTimers)
        {
            hasStartedRaceTimers = true;
            StartRace();
            StartRaceTimers();
            return;
        }
        if (checkpoint == finishLine && hasStartedRaceTimers)
        {

            if (other.transform.parent.tag == "Player")
            {
                laptrack.finishedLap = true;


             //   if (lapTimer[0] < personalbesttime) { personalbesttime = lapTimer[0]; PlayerPrefs.SetFloat("PersonalBestTime", lapTimer[0]); }

                UpdateScoreUI();
                //UPDATE HIGHSCORE STUFF HERE
                lapTimer[0] = 0;
              //  startTimer = false;
                racerCurrentLapAmount[0]++;

                return;
            }


        }
   

      

        //  Debug.Log(other.gameObject.name + " " + checkpoint.gameObject.name);
        //hit collider
        int cIndex = 0;
        foreach ( BoxCollider item in checkPoints)
        {
            if (item == checkpoint)
            {
                break;
            }
            else
            { cIndex++; }
        }

        // search for entry in racers
        int rIndex = 0;
        foreach (GameObject item in racers)
        {
            if (item == other.gameObject)
            {
                UpdateRacerCheckpoint(rIndex, cIndex);
                break;
            }
            else { rIndex++; }
        }

        if (checkpoint == finishLine) { racerCurrentLapAmount[rIndex]++; }
    }

    void UpdateRacerCheckpoint(int racerIndex, int checkpointIndex )
    {

        //check to see if we skipped any checkpoints
        if (checkpointIndex != 0 && checkpointIndex != racerCheckpointindex[racerIndex]+1 && checkpointIndex > racerCheckpointindex[racerIndex])
        {
            Debug.Log(racers[racerIndex].gameObject.name + " skipped " + (checkpointIndex - racerCheckpointindex[racerIndex]) + " checkpoints!");

            // SET THIS UP TO EFFECT SPECIFIC RACERS INSTEAD OF PLAYER
            lapTimer[racerIndex] += 60 * checkpointIndex - racerCheckpointindex[racerIndex];

            if (racerIndex == 0)
            {
                seconds += 10 * checkpointIndex - racerCheckpointindex[racerIndex];
                minutes += 1 * (int)((checkpointIndex - racerCheckpointindex[racerIndex]) * 0.25f);
                UpdateTimer();
            }
        }
        //update racer's current checkpoint
        racerCheckpointindex[racerIndex] = checkpointIndex;

        if (checkpointIndex == checkPoints.Length)
        {
            // we just hit the finish line
            racerCurrentLapAmount[racerIndex]++;
            lapTimer[racerIndex] = 0;
        }
    
    }

    void UpdateTimer()
    {
        // keep track of the overall track time

        for (int i = 0; i < lapTimer.Length; i++)
        {
            lapTimer[i] += 1 ;
        }

       
        miliseconds += Time.unscaledDeltaTime * 100;
        if (miliseconds >= 100) { seconds += 1; miliseconds -= 100; }
        if (seconds >= 60) { minutes += 1; seconds -= 60; }
        minutes = Mathf.Clamp(minutes, 0, 999);

        lapTimerUI.text = (minutes).ToString("00") + ":" + (seconds).ToString("00") + ":" + (miliseconds).ToString("00");

    }

    void CheckDistanceToCheckpoint(int i)
    {

        //TODO: REPLACE DISTANCE TO CHECKPOINT CHECK
      //  DistanceToNextCheckpoint[i] = Vector3.Distance(checkPoints[racerCheckpointindex[i] + 1].transform.position, racers[i].transform.position);

        Vector3 v1 = checkPoints[racerCheckpointindex[i] + 1].transform.position;
        Vector3 v2 = racers[i].transform.position;

        DistanceToNextCheckpoint[i] = Vector3.Distance(v1, v2) * (Vector3.Dot(v1, v2) / (v1.magnitude * v2.magnitude));
    }

    void TrackRacerPlacements()
    {

        // update closest to each checkpoint
        for (int i = 0; i < racers.Length; i++)
        {
            if (racerCheckpointindex[i] + 1 < checkPoints.Length)
            {
                CheckDistanceToCheckpoint(i);
            }
        }

        //step 1: get racer checkpoints passed
        for (int i = 0; i < racers.Length; i++)
        {
            tempIntArray[i] = (racerCurrentLapAmount[i] * checkPoints.Length) + racerCheckpointindex[i];

        }

        List<Vector3> sortingList = new List<Vector3>();

        // store all values into a single list, so we can sort their placements easier

        for (int i = 0; i < racers.Length; i++)
        {
            sortingList.Add( new Vector3(i, tempIntArray[i], DistanceToNextCheckpoint[i]));
        }

        //b to a is highest to lowest, so this is most checkpoints passed to least
        sortingList.Sort((b,a) => a.y.CompareTo(b.y));
       // Debug.Log(sortingList[0] + " " + sortingList[10]);
       
        // this is checkpoints, and racers that are moving towards each checkpoint.
        Vector3[,] arrayOfLists = new Vector3[checkPoints.Length * lapsToFinish, racers.Length];


        // sort the vectors into lots by their y value (        structure should look like  0   1   0   4   5   <checkpoints
                                                            //                                  2           6   <racers in that checkpoint
                                                            //                                  3
        
        int index = 0; // this index is to keep track of placement for each checkpoint    

        for (int i = 0; i < checkPoints.Length * lapsToFinish; i++)            
        {

            //go through each checkpoint (i) and iterate through the racers list (j), adding each racer in order

            for (int j = 0; j < racers.Length; j++)
            {
                if ((int)sortingList[j].y == i)
                {
                    arrayOfLists[i, index] = sortingList[j];
                    index++;
                }         
            }

            //reset the placement index, go through next checkpoint number
            index = 0;

        }

        List<Vector3> tempSortList = new List<Vector3>();

        // placement index
        int Pindex = 12;

        for (int i = 0; i < racers.Length; i++)
        {
            racerPlacementArray[i] = 12 - i; // initialize array with 1-12
        }

        //sort the values by their z value
        for (int i = 0; i < arrayOfLists.GetLength(0); i++)
        {

           
            tempSortList.Clear();

            for (int j = 0; j < racers.Length; j++)
            {
                tempSortList.Add(arrayOfLists[i, j]);
            }

            //sort by distances, lowest to highest
            tempSortList.Sort((b,a) => a.z.CompareTo(b.z));
            

            for (int j = 0; j < racers.Length; j++)
            {
                // add players by the x player index

                if (tempSortList[j].z > 0)
                {
                    //this is adding by closest to their current checkpoint
                    racerPlacementArray[(int)tempSortList[j].x] = Pindex;
                    if (Pindex == 1 ) { playerInFirst = (int)tempSortList[j].x; }
                    Pindex--;
                }
            }
        }

       
    }
}
