﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapController : MonoBehaviour {

    public Camera thisCam;
    public Camera blipCam;

    int timer = 60;

	// Use this for initialization
	void Start () {
        //  thisCam.depthTextureMode = DepthTextureMode.Depth;
        blipCam.clearFlags = CameraClearFlags.Depth;
        
	}
	
	// Update is called once per frame
	void Update () {
        timer--;
        if (timer <=0)
        {
            timer = 200;
            blipCam.gameObject.SetActive(false);
        }
        if (!blipCam.isActiveAndEnabled) { blipCam.gameObject.SetActive(true); }
	}
}
