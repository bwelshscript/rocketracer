﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;


namespace TerrainSlopePainter
{
	/// <summary>
	/// TerrainSlopePainter v1.0
	/// </summary>
	public class TerrainSlopePainter : EditorWindow
	{
		// Add to the menu.
		[MenuItem ("Tools/Terrain Slope Painter")]
		public static void  ShowWindow()
		{
			var t = GetWindowWithRect<TerrainSlopePainter>(new Rect(0,0,265,315),true,"Terrain Slope Painter");

			// Find terrain.
			t.SetActiveTerrain();
		}



		private struct NormalInfoTable
		{
			public SplatPrototype splat;
			public float value;

			public NormalInfoTable(SplatPrototype _splat, float _value)
			{
				splat = _splat;
				value = _value;
			}
		}



		#region variables
		private Terrain target;
		private Vector2 scroll;

		private Dictionary<int,NormalInfoTable> table = new Dictionary<int,NormalInfoTable>();

		private Rect graphRect = new Rect(0,20,360,360);

		private Rect windowRect = new Rect(0,20,64,95);
		private Rect baseRect = new Rect(0,0,64,95);
		private Rect texRect = new Rect(0,15,64,64);
		private Rect normalRect = new Rect(0,80,60,15);

		private Vector3 BezierStart = new Vector3(0,218,0);
		private Vector3 BezierSTan = new Vector3(99,218,0);
		private Vector3 BezierEnd = new Vector3(198,20,0);
		private Vector3 BezierETan = new Vector3(198,119,0);

		private const float dist = 200;

		private Rect buttonRect = new Rect(200,250,65,32);
        private Rect buttonRect2 = new Rect(200, 230, 65, 20);


        // from ver 1.1
        private Rect minmaxhRect = new Rect(5,145,120,10);
		private Rect minmaxvRect = new Rect(140,30,120,10);

		private Rect boxRect = new Rect(5,25,120,120);
		private Rect boxdrawRect = new Rect(5,25,120,120);
		private Color boxdrawColor = new Color(0f,1f,0f,0.3f);

		private Vector2 rotatePivot = new Vector2(145,30);

		private float[] minmaxData = new float[]{0f,1f,0f,1f};

		private Rect undoRect = new Rect(200,283,65,32);
		private float[,,] alphaBuffer = new float[,,]{};
		#endregion



		#region functions
		/// <summary>set active terrain.</summary>
		private void SetActiveTerrain()
		{
			if(Terrain.activeTerrain==null)
			{
				target = FindObjectOfType<Terrain>();
				if(target==null)
					return;
				if(target.terrainData==null)
					target = null;
			}
			else
			{
				target = Terrain.activeTerrain;
				if(target==null)
					return;
				if(target.terrainData==null)
					target = null;
			}
		}

        private void DoPaintAll()
        {
            foreach (Terrain item in FindObjectsOfType<Terrain>())
            {
                target = item;
                DoPaint();
                
            }
          //  EditorUtility.DisplayDialog("Terrain Stratum Painter", "Painting has been finished.", "ok");
        }
		/// <summary>Painting process.</summary>
		private void DoPaint(  )
		{
           
                Debug.Log("Begining paint on " + target.name);


                var td = target.terrainData;
                var ar = target.terrainData.alphamapResolution;
                var alpha = td.GetAlphamaps(0, 0, ar, ar);

                var table2 = table.OrderByDescending(xx => xx.Value.value);

                // set undo buffer.
                alphaBuffer = (float[,,])alpha.Clone();

                // paint all alphamaps.
                for (int y = 0; y < ar; y++)
                {
                    // target area check.
                    var ny = (float)(ar - y) / ar;
                    if (ny < minmaxData[2] || ny > minmaxData[3])
                        continue;

                    for (int x = 0; x < ar; x++)
                    {
                        // target area check.
                        var nx = (float)x / ar;
                        if (nx < minmaxData[0] || nx > minmaxData[1])
                            continue;

                        var normal = td.GetInterpolatedNormal((float)x / ar, (float)y / ar); // be careful of x & y.
                        var angle = Vector3.Angle(Vector3.up, normal) * Mathf.Deg2Rad;

                        var oldA = float.MaxValue;
                        bool hasChanged = false;

                        foreach (var t2 in table2)
                        {
                            var newA = t2.Value.value;
                            if (angle <= oldA && angle > newA)
                            {
                                hasChanged = true;
                                alpha[y, x, t2.Key] = 1f;
                            }
                            else
                                alpha[y, x, t2.Key] = 0f;
                            oldA = newA;
                        }
                        if (!hasChanged)
                            alpha[y, x, table2.Last().Key] = 1f;
                    }
                }

                td.SetAlphamaps(0, 0, alpha);

            
			

		}
		#endregion



		#region GUI
		/// <summary>show window.</summary>
		private void OnGUI()
		{
			EditorGUIUtility.labelWidth = 80;
			target = EditorGUILayout.ObjectField("Target : ",target,typeof(Terrain),true) as Terrain;

			// check terrain.
			if(target==null)
				return;
			var t = target;
			if(t.terrainData == null)
			{
				EditorGUI.LabelField(graphRect,"not found terrain data.");
				return;
			}

			// get splat types.
			var splat = t.terrainData.splatPrototypes;

			// check textures.
			if(splat.Length==0)
			{
				EditorGUI.LabelField(graphRect,"no textures found.");
				return;
			}

			// set table.
			if(splat.Length!=table.Count)
			{
				table.Clear();
				var angle = 90f/splat.Length*Mathf.Deg2Rad;
				for(var i=0;i<splat.Length;i++)
					table.Add(i,new NormalInfoTable(splat[i],angle*i));
			}

			// Draw BezierLine.
			Handles.DrawBezier(
				BezierStart,
				BezierEnd,
				BezierSTan,
				BezierETan,
				Color.white,
				Texture2D.whiteTexture,
				2f
			);

			// show a button before show window.
			if(GUI.Button(buttonRect,"do\npainting"))
				DoPaint();

            if (GUI.Button(buttonRect2, "paint all"))
                DoPaintAll();

            // begin sub-window area.
            BeginWindows();
			{
				// show window, each of splat types.
				for (int i=0;i<table.Count;i++)
				{
					var ta = table[i];
					var r = windowRect;
					r.x = Mathf.Sin(ta.value)*dist;
					r.y = Mathf.Cos(ta.value)*dist+20;

					// draw small red line.
					Handles.color = Color.red;
					var rPos = new Vector3(r.x,r.y,0);
					Handles.DrawLine(rPos,(((Vector3)graphRect.position-rPos).normalized*10)+rPos);

					r = GUI.Window(i,r,(x)=>{
						GUI.DragWindow(baseRect);

						// show texture.
						GUI.DrawTexture(texRect,splat[x].texture,ScaleMode.StretchToFill,false);

						// show height.
						GUI.Label(normalRect,"∠ "+(ta.value*Mathf.Rad2Deg).ToString("F2"));
					},splat[i].texture.name);

					ta.value = Mathf.Atan2(r.x,r.y-20f);
					ta.value = Mathf.Clamp(ta.value,0f,90f*Mathf.Deg2Rad);

					table[i] = ta;
				}
			}
			EndWindows();


			// from ver 1.1
			if(alphaBuffer.Length>0)
			{
				if(GUI.Button(undoRect,"Undo"))
				{
					t.terrainData.SetAlphamaps(0,0,alphaBuffer);
					alphaBuffer = new float[,,]{};
				}
			}

			GUI.Box(boxRect,"Target Area");

			boxdrawRect.xMin = Mathf.Lerp(boxRect.xMin,boxRect.xMax,minmaxData[0]);
			boxdrawRect.xMax = Mathf.Lerp(boxRect.xMin,boxRect.xMax,minmaxData[1]);
			boxdrawRect.yMin = Mathf.Lerp(boxRect.yMin,boxRect.yMax,minmaxData[2]);
			boxdrawRect.yMax = Mathf.Lerp(boxRect.yMin,boxRect.yMax,minmaxData[3]);
			EditorGUI.DrawRect(boxdrawRect,boxdrawColor);

			EditorGUI.MinMaxSlider(minmaxhRect,ref minmaxData[0],ref minmaxData[1],0f,1f);
			GUIUtility.RotateAroundPivot(90f, rotatePivot);
			EditorGUI.MinMaxSlider(minmaxvRect,ref minmaxData[2],ref minmaxData[3],0f,1f);		}
		#endregion
	}
}