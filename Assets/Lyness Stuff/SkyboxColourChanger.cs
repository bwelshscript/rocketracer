﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxColourChanger : MonoBehaviour
{
    public Gradient skyboxGradient;

    //private float intensity = 1.0f;
    public float gradientChangeSpeed = 250f;
    private float gradientPosition;

    // Use this for initialization
    void Start()
    {
        gradientPosition = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        gradientPosition += Time.deltaTime / gradientChangeSpeed;
        if (gradientPosition >= 1.0f)
        {
            gradientPosition = 0.0f;
        }
        SetSkyboxColour();
    }

    private void SetSkyboxColour()
    {
        RenderSettings.skybox.SetColor("_SkyTint", skyboxGradient.Evaluate(gradientPosition));
    }
}
