

TO CREATE A RACE TRACK THE EZ PZ WAY

1: 	Duplicate the base scene. Name it whatever you like, 
	preferably not "TEST" or "MYNAME_TEST_1", if you can avoid it, as it will be used to name your map and its associated files

2:	(If the populate terrains window is not already open)
		GOTO: Tools > Instantiate Terrains

				||||| S A V E  O F T E N |||||

3: 	In the SCENE HEIRACHY, GOTO: Base_Map_Objects > terrain
		dont change the map height yet, or it will be applied to every terrain
		do add the detail textures to the terrain you wish to use for your track ( e.g. grass colour, cliff colour, detail colours)
		add any other assets you will use, like trees and grasses.

4: 	In the populate terrains window, place the terrain (from step 3) in the Base Terrain field
	in width and height set the amount of terrains you wish to create 
			(for example the bubblegum desert used 8x2, and had a lap time of ~2mins 30sec, this is a medium size)

	If you need to repopulate your scene (starting again) GOTO: ASSETS/GENERATED ASSETS/[your scene name]
		and delete the terrain data files in there. you could also just delete the folder.
		then delete the generated terrains in the scene itself.
		either keep a terrain to use as the base information or better yet, just re-duplicate the scene from the start.

5: 	go through and make your changes to the terrain height for each terrain tile

6:	DELETE THE BASE TERRAIN from step 3 (stitch terrain doesnt work well with odd numbers or terrains that are not placed next to each other correctly)
	GOTO: GameObject > Stitch Terrains...
	Select "Autofill from scene"
	Select "Stitch"
	this will stitch the terrains together, the tool's readme is under
		Assets/Imported Assets/Stitchscape/Documentation

7:	GOTO: Tools > Terrain Slope Painter
	The main terrain should have autofilled, but if not, place it into the terrain box up the top of the window
	move the terrain detail textures to the appropriate slope angle 
		(eg: a grass texture would be at 0, on the left, while a rocky cliff texture would be around 70, to the right)
	
	if you are testing out values, select "do paint" to only paint one terrain
	else select "paint all" to paint all terrains.

	this operation will take a long time for each terrain, as it goes through each pixel and decides 
	what its value will be based on its normal. We have to use high resolution detail maps to get this process 
	to look any good as there is no blending in this tool. This also means that you may get several "file larger than 10mb"
	warnings when trying to merge, just accept it.
	tool information files are found at
		Assets\Imported Assets\TerrainSlopePainter

8:	Now you need to place the start, finish and inbetween checkpoints for your map. there should be some placed already,
	that will be leinked into the player and other controlling objects. just move them to where they should be. 
	Dont overlap the start and finish lines, and dont place the player inside the start line (they should get a running start)
	Do keep the checkpoints in sequential order as it will make adding them to the race controller much easier.
	
	Once you have finished placing your checkpoints, they need to be placed in the race controller (Controlling objects > raceController)
	It contains a Check Points array, which already should contain "Start, Checkpoint (1), Checkpoint (2), Finish"
	continue this convention by setting size to 0, locking the inspector, then selecting all checkpoints and dragging them onto the
	"Check Points" name, adding all checkpoints at once and in sequential order.


9: 	Now that you have a nice layout, move the playerparts and cameras objects to the start position of your map and hit play,
	to test out your track layout.

	